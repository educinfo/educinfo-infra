# EDUCINFO INFRA

Ce projet héberge l'infrastructure de base pour le projet educinfo

Il comprend

 * la page d'acceuil du site [Educinfo](https://lycee.educinfo.org/)
 * les quelques fonctions PHP qui fabriquent le site à partir des fichiers de configuration
 * les principaux templates twig qui créent le site
 * les modules javascripts nécessaires à l'interactivité et à l'exécution du code
 * les scripts pour la mise en place
