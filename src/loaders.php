<?php

require_once 'lib/site/functions.php';

if (isset($config['twig_directory'])) {
	require_once($config['twig_directory']."/autoload.php");
} else {
	// default on debian
	require_once 'Twig/autoload.php';
}

$templates_directory = (isset($config['templates_directory'])) ? $config['templates_directory'] : '/srv/web/lycee/templates';

$loader = new \Twig\Loader\FilesystemLoader($templates_directory);
$twig = new \Twig\Environment($loader,['cache'=>false]);

// Parcoure la liste des activites
//$activites = array();
//foreach(glob("ressources/activites/*php") as $filename) include_once($filename);
if (isset($_GET['activite']) && file_exists("ressources/".$activites[$_GET['activite']]['directory']."/scripts.php")) {
	require_once("ressources/".$activites[$_GET['activite']]['directory']."/scripts.php");
}

$action = (isset($_GET['action'])) ? $_GET['action'] : 'none';

$variables=array();

switch ($action) {
	case 'video':
		if (isset($_GET['id'])) {
			if (isset($_GET['activite']) && file_exists("ressources/".$activites[$_GET['activite']]['directory']."/videos.php")) {
				require_once("ressources/".$activites[$_GET['activite']]['directory']."/videos.php");
			}
			$id=$_GET['id'];
			$variables['video']=$videos[$id];
			$variables['is_local_ip']=$IS_LOCAL_IP;
			$template = $twig->load('video.ajax.twig');
			$template->display($variables);
		}
	break;
	case 'javascript':
		if (isset($_GET['id'])){
			$id=$_GET['id'];
			$variables['javascript']=$examples_scripts[$id];
			$variables['script_id']=$id;
			if (isset($_GET['activite']))
				$variables['activite']=$_GET['activite'];
			$template = $twig->load('javascript.ajax.twig');
			$template->display($variables);
		}
		break;
	case 'p5js':
		if (isset($_GET['id'])){
			$id=$_GET['id'];
			$variables['javascript']=$examples_scripts[$id];
			$variables['script_id']=$id;
			if (isset($_GET['activite']))
				$variables['activite']=$_GET['activite'];
			$template = $twig->load('javascript.ajax.twig');
			$template->display($variables);
		}
		break;
	case 'scripts':
		header('Content-Type: application/json');
		if (isset($_GET['id'])) {
			$id=$_GET['id'];
		} else {
			$id='default';
		}
		echo(json_encode($examples_scripts[$id]));
		break;
	case 'none':
	default:
}
