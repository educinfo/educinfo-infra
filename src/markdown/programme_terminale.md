# Programme de Spécialité NSI
## Classe de terminale

Les connaissances en italique sont des _entrées non prépondérantes_ qui ne conduiront pas à des questions pour 
l'examen pour l'année scolaire 2020/2021. 

### Histoire de l'informatique

 * _Évènements clefs de l'histoire de l'informatique_ <span class="badge bg-secondary">Allégement</span>

### Structures de données

 * Structures de données, interface et implémentation <span class="badge bg-success">Fait</span>
 * Vocabulaire de la progammation objet : classes, attributs, méthodes et objets <span class="badge bg-success">Fait</span>
 * Structures linéaires : listes, piles et files <span class="badge bg-success">Fait</span>
 * Dictionnaires, index et clefs <span class="badge bg-success">Fait</span>
 * Structures hiérarchiques : arbres <span class="badge bg-success">Fait</span>
 * Arbres binaires (nœuds, racines, feuilles, sous-arbresgauches, sous-arbres droits) <span class="badge bg-success">Fait</span>
 * _Structures relationnelles : graphes. Sommets, arcs, arêtes, graphes orientés ou nonorientés._ <span class="badge bg-secondary">Allégement</span>
  
### Bases de données

 * Modèle relationnel:relation, attribut, domaine, clef primaire, clef étrangère, schéma relationnel <span class="badge bg-success">Fait</span>
 * Base de données relationnelle <span class="badge bg-success">Fait</span>
 * _Système de gestion de bases de données relationnelles_ <span class="badge bg-secondary">Allégement</span>
 * Langage SQL:requêtes d’interrogation et de mise à jour d’une base de données <span class="badge bg-success">Fait</span>

### Architectures matérielles, systèmes d’exploitation et réseaux

 * Composants intégrés d’un système sur puce <span class="badge bg-primary">À faire</span>
 * Gestion des processus et des ressources par un système d’exploitation <span class="badge bg-primary">À faire</span>
 * Protocoles de routage <span class="badge bg-success"> Fait </span>
 * _Sécurisation des communications_ <span class="badge bg-secondary">Allégement</span>

### Langages et programmation

 * _Notion de programme en tant que donnée.Calculabilité, décidabilité_  <span class="badge bg-secondary">Allégement </span>
 * Récursivité <span class="badge bg-success">Fait</span>
 * Modularité <span class="badge bg-success">Fait</span>
 * _Paradigmes de programmation_ <span class="badge bg-secondary">Allégement</span>
 * Mise au point des programmes.Gestion des bugs <span class="badge bg-warning"> En cours </span>

### Algorithmique

 * Algorithmes sur les arbres binaires et sur les arbres binaires de recherche <span class="badge bg-success">Fait</span>
 * Méthode «diviser pour régner» <span class="badge bg-success"> Fait </span>
 * _Programmation dynamique_ <span class="badge bg-secondary">Allégement</span>
 * _Recherche textuelle_ <span class="badge bg-secondary">Allégement</span>