# Programme de Spécialité NSI
## Classe de première

### Histoire de l'informatique

 * _Évènements clefs de l'histoire de l'informatique_ <span class="badge bg-secondary">Allégement</span>

### Représentation des données : types et valeurs de base

 * Écriture d’un entier positif dans une base b &ge; 2 <span class="badge bg-success">Fait</span>
 * Représentation binaire d’un entier relatif <span class="badge bg-success">Fait</span>
 * Représentation approximative des nombres réels: notion de nombre flottant <span class="badge bg-primary">À faire</span>
 * Valeurs booléennes: 0, 1. Opérateurs booléens: and, or, not.Expressions booléennes <span class="badge bg-success">Fait</span>
 * Représentation d’un texte en machine.Exemples des encodages ASCII, ISO-8859-1,Unicode <span class="badge bg-primary">À faire</span>
  
### Représentation des données: types construits

 * p-uplets.p-uplets nommés <span class="badge bg-primary">À faire</span>
 * Tableau indexé, tableau donné en compréhension <span class="badge bg-warning"> En cours </span>
 * Dictionnaires par clés et valeurs <span class="badge bg-primary">À faire</span>

### Traitement de données en tables
 
 * Indexation de tables <span class="badge bg-primary">À faire</span>
 * Recherche dans une table <span class="badge bg-primary">À faire</span>
 * Tri d’une table <span class="badge bg-primary">À faire</span>
 * Fusion de tables <span class="badge bg-primary">À faire</span>

### Langages et programmation

 * Constructions élémentaires <span class="badge bg-success">Fait</span>
 * Diversité et unité des langages de programmation <span class="badge bg-warning"> En cours </span>
 * Spécification <span class="badge bg-warning"> En cours </span>
 * Mise au point de programmes <span class="badge bg-warning"> En cours </span>
 * Utilisation de bibliothèques <span class="badge bg-success">Fait</span>

### Algorithmique

 * Parcours séquentiel d’un tableau <span class="badge bg-success">Fait</span>
 * Tris par insertion, par sélection <span class="badge bg-warning"> En cours </span>
 * Algorithme des k plus proches voisins <span class="badge bg-primary">À faire</span>
 * Recherche dichotomique dans un tableau trié <span class="badge bg-success">Fait</span>
 * Algorithmes gloutons <span class="badge bg-primary">À faire</span>