# Memento Base de données

## gestion des relations

Pour consulter des données, ajouter une entrée, modifier une entrée ou supprimer une entrée dans une base de données relationnelle, on effectue des requêtes grâce au langage SQL ( Structured Query Language, c’est à dire Langage de requêtes structurées)

**CREATE TABLE** : utilisé pour créer une table, en donnant le nom de la table et les attributs

```sql
CREATE TABLE livres (id INT PRIMARY KEY, titre TEXT, auteur TEXT,annee_publi INT ,note INT);
```

**INSERT**  : utilisé pour ajouter un enregistrement, en donnant la table, les attributs concernés et leurs valeurs

```sql
INSERT INTO livres (id,titre,auteur,annee_publi,note) VALUES (15,'1984','Orwell',1949,10);
```

**SELECT** : utilisé pour effectuer une recherche dans une table (FROM) en indiquant les clauses (WHERE).

```sql
SELECT titre,auteur FROM livres WHERE auteur='Asimov';
```

*NB : On peut ordonner les résultats en utilisant la clause **ORDER BY** (par exemple ORDER BY ann_publi).*

**UPDATE**: utilisé pour modifier un enregistrement existant, en indiquant les modifications (avec SET) et les clauses (avec WHERE) pour indiquer les enregistrements à modifier

```sql
UPDATE livres SET note=7 WHERE titre='Hypérion';
```

**DELETE** : utilisé pour supprimer un enregistrement, en donnant le nom de la table et les clauses

```sql
DELETE FROM livres WHERE titre="Un mauvais choix";
```

**INNER JOIN** : utilisé pour créer une table temporaire, en faisant la jointure de deux tables

```sql
SELECT livres.titre, auteurs.nom, auteurs.prenom
FROM livres INNER JOIN auteurs ON livres.id_auteur = auteurs.id
WHERE auteurs.nom = "Dick" AND auteurs.prenom="Philip K.";
```

*NB : On peut avoir plusieurs **INNER JOIN** à la suite si on relie entre elles plus de deux tables.*

## Gestion des attributs

Lorsque l’on crée les attributs pour une nouvelle table, on peut ajouter certains mots clefs qui modifient le  comportement :

* **NOT NULL** : l’attribut doit nécessairement avoir une valeur (il ne peut pas avoir la valeur NULL, qui signifie « non renseigné »
* **PRIMARY KEY** : l’attribut est la clef primaire de la table
* **AUTO_INCREMENT** : si la clef primaire est un entier, cette option permet d’indiquer que la clef augmente automatiquement (on a pas besoin de préciser sa valeur)

## Opérateur LIKE en SQL

On peut faire une recherche sur une partie d'un chaîne de caractère. C'est là qu'intervient l'opérateur **LIKE**. Il permet d'utiliser une clause **WHERE** qui recherchera tout ce qui correspond à un motif. Deux caractères spéciaux peuvent être utilisés dans une recherche avec **LIKE** :

* le caractère pourcent % qui remplace n'importe quelle chaine de caractère, y compris vide
* le caractère souligné \_ remplace un et un seul caractère

```sql
SELECT FROM livres WHERE titre LIKE '%nuit%';
```