<?php

$datas=array();
$categories=array();
$sections=array();
$activites = array();

require_once 'lib/site/functions.php';
if (isset($config['twig_directory'])) {
	require_once($config['twig_directory']."/autoload.php");
} else {
	// default on debian
	require_once 'Twig/autoload.php';
}

$templates_directory = (isset($config['templates_directory'])) ? $config['templates_directory'] : '/srv/web/lycee/templates';


// Acquisition des paramètres

$page = (isset($_GET['page'])) ? $_GET['page'] : 'accueil';

$integrated = (isset($_GET['integrated'])) ? $_GET['integrated'] : 'false';

$datas = $pages[$page];

if (isset($_GET['activite'])) {
	$datas['activite']=$_GET['activite'];
}

$variables['is_local_ip']=$IS_LOCAL_IP;
$datas['pages']=$pages;

if (isset($menu_activite)) {
	$datas['sommaire']=$menu_activite;
} else {
	$datas['sommaire']=[];
}
	
/*
if (isset($datas['page_parente']) ) {
	$datas['nom_page_parente'] = $pages[$datas['page_parente']]['titre'];
}
*/

if (isset($datas['page_precedente']) ) {
	$datas['nom_page_precedente'] = $pages[$datas['page_precedente']]['titre'];
}

if (isset($datas['page_suivante']) ) {
	$datas['nom_page_suivante'] = $pages[$datas['page_suivante']]['titre'];
}

if (isset($datas['page_enfant']) ) {
	$datas['nom_page_enfant'] = $pages[$datas['page_enfant']]['titre'];
}

if (isset($datas['category'])) {
}


// Consolidation des données 

$datas['categories']=$categories;
$datas['sections']=$sections;
$datas['activites']=$activites;
$datas['config']=$config;
$data['BASTHON_CACHE_BUSTING_TIMESTAMP']='1632348240';
$datas['integrated']=$integrated;

// Affichage du template

$loader = new \Twig\Loader\FilesystemLoader($templates_directory);
$twig = new \Twig\Environment($loader,['cache'=>false]);

$template = $twig->load($pages[$page]['template']);
$template->display($datas);
