/* jshint browser: true, esversion: 10  */

window.markdowns = (function () {

    that = {};

    that.ajaxLoad = function(url, type = 'GET', data = null) {
        return new Promise((resolve, reject) => {
            var xhttp = new XMLHttpRequest();
            xhttp.responseType = "text";
            xhttp.open(type, url, true);
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4) {
                    if (this.status == 200) 
                        resolve(this.responseText);
                    else 
                        reject(Error(this.statusText));
                }
            };
            xhttp.onerror = function () {
                reject(Error("Erreur réseau"));
            };
            xhttp.send(data);
        });
    };

    that.loadMD = async function(url, query = '#MD') {
        that.ajaxLoad(url).then(text => {
            let converter = new showdown.Converter();
            converter.setOption('tables', true);
            document.querySelector(query).innerHTML = converter.makeHtml(text);
            if (that.highlight) {
                hljs.highlightAll();
                }
            document.dispatchEvent(new Event('markdownLoaded'));
        }).catch(myError => {
            console.log(`erreur dans le chargement de ${url} : ${myError}`);
        });
    };

    return that;
})();