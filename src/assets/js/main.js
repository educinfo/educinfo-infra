/*jshint browser: true, esversion:10, sub:true , node: true */
/*globals basthonKernelLoader, bootstrap, makeShell, makeEditor, makeGraphics, makeGUI, hljs */
"use strict";

basthonKernelLoader.kernelLoaded().then(
    () => document.dispatchEvent(new Event("BasthonLoaded"))
);

document.addEventListener("BasthonLoaded",
    () => {
        window.shell = makeShell();
        window.editor = makeEditor();
        window.graphics = makeGraphics();
        window.gui = makeGUI();
        
        window.basthonMode = 'script';
        document.pythonModal = new bootstrap.Modal(document.getElementById('pythonModal'));
        
        window.editor.setFontSize(10);
        
        let buttons = document.querySelectorAll('.btn-python');
        buttons.forEach(function (button) {
            button.addEventListener('click', window.gui.openModal);
        });
        
        document.getElementById('pythonModal').addEventListener('hidden.bs.modal', function (event) {
            window.editor.backup(window.gui.pythonId);
        });
        
        document.getElementById('localDownload').addEventListener('show.bs.dropdown', function (event) {
            window.gui.propose_downloads();
        });
        
        hljs.highlightAll();
        
        window.gui.init();       
    }    
);