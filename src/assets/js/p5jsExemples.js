var examples = {
  init: function (file) {
    if (document.getElementById('exampleFrame') !== null) {

      // Editor
      console.log('Création de l\'éditeur pour les exemples p5js')
      examples.editor = ace.edit('htmEditor');
      examples.editor.$blockScrolling = Infinity;
      examples.editor.setTheme('ace/theme/monokai');
      examples.editor.getSession().setMode('ace/mode/javascript');
      examples.editor.getSession().setTabSize(2);
      examples.editor.setShowPrintMargin(false);
      examples.editor.setHighlightActiveLine(true);
      examples.editor.renderer.setShowGutter(true);

      examples.dims = [];

      // Button

      $('#runButton').click(function () {
        examples.runExample();
      });
      $('#resetButton').click(function () {
        examples.resetExample();
      });


      // Example Frame
      if ($("#isMobile-displayButton").length == 0) {
        //it not mobile

        $('#exampleFrame').on('load', function () {
          examples.loadExample(false);
        });
      } else {
        $('#isMobile-displayButton').click(function () {

          $('#exampleFrame').show();
          $('#exampleFrame').ready(function () {
            // alert('exampleFrame load')
            examples.loadExample(true);
          });

        });


      }

      // Capture clicks

      $.ajax({
        url: file,
        dataType: 'text'
      })
        .done(function (data) {
          $('#exampleSelector').hide();


          var ind = data.indexOf('*/');
          data = data.substring(ind + 3);
          examples.resetData = data;
          examples.Data = data;
          examples.showExample();
        })
    }
  },

  updateExample: function (file) {
    $.ajax({
      url: file,
      dataType: 'text'
    })
      .done(function (data) {
        $('#exampleSelector').hide();

        examples.Data = data;

        examples.showExample();
      })
  },

  showExample: function () {
    examples.editor.getSession().setValue(examples.Data);

    //resize height of editor
    var rows = examples.editor.getSession().$rowLengthCache.length;
    var lineH = examples.editor.renderer.lineHeight;
    $('#exampleEditor').height(rows * lineH + 'px');

    examples.runExample();
    $('#exampleDisplay').show();
  },
  // display iframe
  runExample: function () {
    $('#exampleFrame').attr('src', $('#exampleFrame').attr('src'));
  },
  resetExample: function () {
    examples.Data = examples.resetData;
    examples.showExample();
  },
  downloadExample: function () {
    var link = document.createElement('a');
    link.download = "sketch.js";
    link.href = 'data:text/javascript,' + examples.editor.getSession().getValue();
    link.click();
  },
  // load script into iframe
  loadExample: function (isMobile) {
    var exampleCode = examples.editor.getSession().getValue();

    try {

      if (exampleCode.indexOf('new p5()') === -1) {
        exampleCode += '\nnew p5();';
      }

      if (isMobile) {

        $('#exampleFrame').css('position', 'fixed');
        $('#exampleFrame').css('top', '0px');
        $('#exampleFrame').css('left', '0px');
        $('#exampleFrame').css('right', '0px');
        $('#exampleFrame').css('bottom', '0px');
        $('#exampleFrame').css('z-index', '999');
        // var re = /createCanvas\((.*),(.*)\)/g;
        //   var arr = exampleCode.split(re);
        // var height = $(screen).height();
        // var width = $(screen).width()
        //   $('#exampleFrame').css('height', height+'px');
        //   $('#exampleFrame').css('width', width+'px');
        //   console.log(height + ' ,' + width);
        //exampleCode = exampleCode.replace(/windowWidth/, winWidth).replace(/windowHeight/, winHeight);

        // var userCSS = $('#exampleFrame')[0].contentWindow.document.createElement('style');
        // userCSS.type = 'text/css';
        // userCSS.innerHTML = 'html, body, canvas { width: 100% !important; height: 100% !important;}';
        //$('#exampleFrame')[0].contentWindow.document.head.appendChild(userCSS);

      } else {
        if (examples.dims.length < 2) {
          var re = /createCanvas\((.*),(.*)\)/g;
          var arr = exampleCode.split(re);
          arr[2] = eval(arr[2]) + 20;
          $('#exampleFrame').height(arr[2] + 'px');
        } else {
          $('#exampleFrame').height(examples.dims[1] + 'px');
        }

      }

      var userScript = $('#exampleFrame')[0].contentWindow.document.createElement('script');
      userScript.type = 'text/javascript';
      userScript.text = exampleCode;
      userScript.async = false;
      $('#exampleFrame')[0].contentWindow.document.body.appendChild(userScript);



    } catch (e) {
      console.log(e.message);
    }
  }
};