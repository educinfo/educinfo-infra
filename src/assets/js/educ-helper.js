/* jshint browser: true, esversion: 10  */

window.educHelper = (function () {
    that = {};

    class blockModule extends HTMLElement {

        constructor(block_type, titre) {
            super();
            this._block_type = block_type;
            this._initial_content = this.innerHTML;
            this.innerHTML = "";
            if (block_type != "python" && block_type != "code") {
                let template = document.getElementById('mes-blocs');
                this.appendChild(template.content.cloneNode(true));
                let inside = this.querySelector('.inside');
                inside.classList.add(this._block_type);
                if (titre == null) {
                    let dataTitre = this.getAttribute('titre');
                    if (dataTitre != null) {
                        if (block_type == "cours") {
                            this._titre = document.createElement('h3');
                        } else {
                            this._titre = document.createElement('h6');
                        }
                        this._titre.innerHTML = dataTitre;
                    } else {
                        this._titre = null;
                    }
                } else {
                    this._titre = titre;
                }
                if (this.dataset.src === undefined) {
                    inside.innerHTML = this._initial_content;
                } else {
                    inside.innerHTML = this.loadData(this.dataset.src);
                }
                if (this._titre != null) {
                    inside.prepend(this._titre);
                }
            }
        }

        loadData(source) {
            let response;
            fetch(source).then(data => response = data);
            if (response === undefined)
                response = '';
            return response;
        }

    }

    class Cours extends blockModule {
        constructor() {
            super('cours');
        }
    }


    /* Elements custom */
    class Attention extends blockModule {
        constructor() {
            let titre = document.createElement('h6');
            titre.innerHTML = 'Attention !';
            super('attention', titre);
        }
    }

    /* Elements custom */
    class Retenir extends blockModule {
        constructor() {
            let titre = document.createElement('h6');
            titre.innerHTML = ' À retenir.';
            super('retenir', titre);
        }
    }

    /* Elements custom */
    class Info extends blockModule {
        constructor() {
            let titre = document.createElement('h6');
            titre.innerHTML = ' Information.';
            super('info', titre);
        }
    }

    /* Elements custom */
    class Exemple extends blockModule {
        constructor() {
            let titre = document.createElement('h6');
            titre.innerHTML = ' Exemple.';
            super('exemple', titre);
        }
    }

    /* Elements custom */
    class Todo extends blockModule {
        constructor() {
            let titre = document.createElement('h6');
            titre.innerHTML = ' À faire.';
            super('todo', titre);
        }
    }

    /* Elements custom */
    class Correction extends blockModule {
        constructor() {
            let titre = document.createElement('h6');
            titre.innerHTML = '  Correction ';
            super('correction', titre);
        }
    }

    /* Elements custom */
    class Abstract extends blockModule {
        constructor() {
            let titre = document.createElement('h6');
            titre.innerHTML = '  Résumé ';
            super('abstract', titre);
        }
    }

    /* Elements custom */
    class Tip extends blockModule {
        constructor() {
            let titre = document.createElement('h6');
            titre.innerHTML = ' Conseil ';
            super('tip', titre);
        }
    }

    /* Elements custom */
    class Succes extends blockModule {
        constructor() {
            let titre = document.createElement('h6');
            titre.innerHTML = '  Succés ';
            super('success', titre);
        }
    }

    /* Elements custom */
    class Echec extends blockModule {
        constructor() {
            let titre = document.createElement('h6');
            titre.innerHTML = '  Échec ';
            super('echec', titre);
        }
    }

    /* Elements custom */
    class Danger extends blockModule {
        constructor() {
            let titre = document.createElement('h6');
            titre.innerHTML = '  Danger ! ';
            super('danger', titre);
        }
    }

    /* Elements custom */
    class Bug extends blockModule {
        constructor() {
            let titre = document.createElement('h6');
            titre.innerHTML = '  Bug ';
            super('bug', titre);
        }
    }

    /* Elements custom */
    class Citation extends blockModule {
        constructor() {
            let titre = document.createElement('h6');
            titre.innerHTML = '  Citation ';
            super('citation', titre);
        }
    }

    /* Elements custom */
    class Afaire extends blockModule {
        constructor() {
            let titre = document.createElement('h6');
            titre.innerHTML = '  À faire ';
            super('afaire', titre);
        }
    }

    /* Elements custom */
    class Consigne extends blockModule {
        constructor() {
            let titre = document.createElement('h6');
            titre.innerHTML = '  À faire ';
            super('consigne', titre);
        }
    }

    /* Elements custom */
    class Resume extends blockModule {
        constructor() {
            let titre = document.createElement('h6');
            titre.innerHTML = '  Résumé ';
            super('resume', titre);
        }
    }

    /* Elements custom */
    class Remarque extends blockModule {
        constructor() {
            let titre = document.createElement('h6');
            titre.innerHTML = '  Remarque ';
            super('remarque', titre);
        }
    }


    class MD {

        constructor() {
            // Extension
            showdown.extension('div', function () {
                return [{
                    type: 'listener',
                    listeners: {
                        'tables.before': function (event, text, options, globals) {
                            return text.replace(/(?:^|\n)(:{3})\s*([_-][a-zA-Z][-_a-zA-Z0-9]*|[a-zA-Z][-_a-zA-Z0-9]+)\n([\s\S]*?)\n\1/gm,
                                function (wholeMatch, delim, className, divBlock, nextChar) {
                                    divBlock = divBlock.replace(/^\n+/g, ''); // trim leading newlines
                                    divBlock = divBlock.replace(/\n+$/g, ''); // trim trailing newlines
                                    let header = "";
                                    let id = '';
                                    let division = '';
                                    switch (className.toLowerCase()) {
                                        case 'attention':
                                            header = '<h6> Attention !</h6>\n';
                                            break;
                                        case 'retenir':
                                            header = '<h6> À retenir.</h6>\n';
                                            break;
                                        case 'info':
                                            header = '<h6> Information.</h6>\n';
                                            break;
                                        case 'todo':
                                            header = '<h6> À faire.</h6>\n';
                                            break;
                                        case 'exemple':
                                            header = '<h6> Exemple.</h6>\n';
                                            break;
                                        case 'correction':
                                            // Create a unique Id for editor
                                            let arr = new Uint8Array(8);
                                            window.crypto.getRandomValues(arr);
                                            id = Array.from(arr, dec => ('0' + dec.toString(16)).substring(0, 2)).join('');
                                            id = 'cor' + id;
                                            console.log(id);
                                            header = '<h6> Correction </h6>\n';
                                            break;
                                    }
                                    if (className.toLowerCase() == 'correction') {
                                        division = '<button class="btn btn-primary collapsed" ';
                                        division = division + 'data-bs-toggle="collapse" data-bs-target="#' + id + '" aria-expanded="false">';
                                        division = division + '<i class="bi bi-bullseye"></i> Afficher la Correction </button>';
                                    }
                                    division = division + '<div class="' + className;
                                    if (id != '') {
                                        division = division + ' collapse hide"';
                                        division = division + ' id="' + id + '"';
                                    } else {
                                        division = division + '"';
                                    }
                                    division = division + '>' + header + divBlock + '\n</div>';
                                    console.log(division);
                                    return division;
                                });
                        }
                    }
                }];
            });

            this._converter = new showdown.Converter({
                extensions: ['div'],
                tables: true
            });
        }


        async inline_convert_all() {
            function highlightElements(element) {
                const codeElements = element.querySelectorAll('pre code');
                codeElements.forEach(el => hljs.highlightElement(el));
            }
            
            function setupModalAndButtons(element) {
                document.javascriptModal = new bootstrap.Modal(document.getElementById('javascriptModal'));
                let jsbuttons = element.querySelectorAll('.btn-javascript');
                jsbuttons.forEach(button => {
                    button.addEventListener('click', () => { document.javascriptModal.show(button); });
                });
            }

            const processData = function(e) {
                return (data) => {
                    e.innerHTML = this._converter.makeHtml(data);
                    highlightElements(e);
                    MathJax.typeset();
                    setupModalAndButtons(e); 
               };
            }.bind(this);

            console.log('convert()');
            hljs.configure({ ignoreUnescapedHTML: true });
            let markdowns = document.getElementsByClassName('md');
            for (let index = 0; index < markdowns.length; index++) {
                const element = markdowns.item(index);
                if (element.dataset.src === undefined) {
                    const content = element.innerHTML;
                    element.innerHTML = this._converter.makeHtml(content);
                    highlightElements(element);
                } else {
                    fetch(element.dataset.src)
                        .then(response => response.text())
                        .then(processData(element));
                }
            }
        }

        // generateId :: Integer -> String
        generateId(len) {
            var arr = new Uint8Array(len / 2);
            window.crypto.getRandomValues(arr);
            return Array.from(arr, this.dec2hex).join('');
        }
    }

    class Code extends blockModule {
        constructor() {
            super('code');
            let template = document.getElementById('mon-code');
            this.appendChild(template.content.cloneNode(true));
            let inside = this.querySelector('code');

            if (this.dataset.src === undefined) {
                inside.innerHTML = this._initial_content;
            } else {
                let text;
                fetch(this.dataset.src)
                    .then((response) => response.text())
                    .then((text) => {
                        inside.innerHTML = text;
                        hljs.configure({ ignoreUnescapedHTML: true });
                        hljs.highlightElement(inside);
                    });

            }

            if (this.dataset.lang === undefined) {
                this.lang = "python";
            } else {
                this.lang = this.dataset.lang;
            }
            inside.classList.add(this.lang);

        }
    }

    class Python extends blockModule {

        constructor() {
            super('python');
            let template = document.getElementById('mon-python');
            this.appendChild(template.content.cloneNode(true));

            // Create a unique Id for editor
            if (this.id == "" || this.id == null) {
                this.id = this.generateId(16);
                console.log(this.id);
            }

            this.datas = {};
            this.datas.initialPython = this._initial_content;

            /* Divisions */
            this._tododiv = this.querySelector('[role="consignes"]');
            this._acediv = this.querySelector('[role="editor"]');
            this._pythondiv = this.querySelector('[role="result"]');
            this._pythonoutput = this.querySelector('[role="output"');
            this._pythonoutput.id = this.id + "Console";
            this._graphicdiv = this.querySelector('[role="graphic"]');
            this._graphicdiv.id = this.id + "Graphic";
            /* Editor buttons */
            this._downloadButton = this.querySelector('[role="download"]');
            this._restoreButton = this.querySelector('[role="restore"]');
            this._saveButton = this.querySelector('[role="save"]');
            this._reloadButton = this.querySelector('[role="reload"]');
            this._runButton = this.querySelector('[role="run"]');
            /* Tab buttons */
            this._consoleTab = this.querySelector('[aria-controls="console"]');
            this._graphicTab = this.querySelector('[aria-controls="graphic"]');

            /* Editor */
            this.editor = ace.edit(this._acediv);
            this.editor.setTheme("ace/theme/monokai");
            this.editor.session.setMode("ace/mode/python");
            if (this.dataset.src === undefined) {
                this.editor.setValue(this.datas.initialPython, 1);
            } else {
                this.editor.setValue(this.loadData(this.dataset.src), 1);
            }

            /* Events */
            this._consoleTab.addEventListener('click', (function (elt) {
                return function (e) {
                    elt._pythondiv.classList.add('show', 'active');
                    elt._graphicdiv.classList.remove('show', 'active');
                    elt._consoleTab.classList.add('active');
                    elt._graphicTab.classList.remove('active');
                    e.preventDefault();
                };
            })(this));

            this._graphicTab.addEventListener('click', (function (elt) {
                return function (e) {
                    elt._pythondiv.classList.remove('show', 'active');
                    elt._graphicdiv.classList.add('show', 'active');
                    elt._consoleTab.classList.remove('active');
                    elt._graphicTab.classList.add('active');
                    e.preventDefault();
                };
            })(this));


            function pythonout(elt) {
                return function (txt) {
                    elt._pythonoutput.innerHTML = elt._pythonoutput.innerHTML + txt;
                };
            }

            function builtinRead(x) {
                if (Sk.builtinFiles === undefined || Sk.builtinFiles.files[x] === undefined) {
                    throw "File not found: '" + x + "'";
                }
                return Sk.builtinFiles.files[x];
            }

            function myRun(elt) {
                console.log(elt);
                console.log(elt.editor.getValue());

                return function () {
                    (function (elt, prog, mypre) {
                        mypre.innerHTML = '';
                        Sk.pre = "output";
                        Sk.canvas = elt._graphicdiv.id;
                        Sk.python3 = true;
                        var SkFuture = {
                            print_function: true,
                            division: true,
                            absolute_import: null,
                            unicode_literals: true,
                            // skulpt specific
                            set_repr: false,
                            class_repr: false,
                            inherit_from_object: false,
                            super_args: false,
                            // skulpt 1.11.32 specific
                            octal_number_literal: true,
                            bankers_rounding: true,
                            python_version: true,
                            dunder_next: true,
                            dunder_round: true,
                            exceptions: true,
                            no_long_type: false,
                            ceil_floor_int: true,
                        };
                        Sk.externalLibraries = {
                            numpy: {
                                path: 'https://cdn.jsdelivr.net/gh/diraison/GenPyExo/external/numpy/__init__.js',
                                dependencies: ['https://cdn.jsdelivr.net/gh/diraison/GenPyExo/external/deps/math.js']
                            },
                            "numpy.random": {
                                path: 'https://cdn.jsdelivr.net/gh/diraison/GenPyExo/external/numpy/random/__init__.js'
                            },
                            matplotlib: {
                                path: 'https://cdn.jsdelivr.net/gh/diraison/GenPyExo/external/matplotlib/__init__.js'
                            },
                            "matplotlib.pyplot": {
                                path: 'https://cdn.jsdelivr.net/gh/diraison/GenPyExo/external/matplotlib/pyplot/__init__.js',
                                dependencies: ['https://cdn.jsdelivr.net/gh/diraison/GenPyExo/external/deps/d3.min.js']
                            },
                            pygal: {
                                path: 'https://cdn.jsdelivr.net/gh/trinketapp/pygal.js@0.1.4/__init__.js',
                                dependencies: ['https://cdn.jsdelivr.net/gh/highcharts/highcharts-dist@6.0.7/highcharts.js',
                                    'https://cdn.jsdelivr.net/gh/highcharts/highcharts-dist@6.0.7/highcharts-more.js'
                                ]
                            },
                            processing: {
                                path: 'https://cdn.jsdelivr.net/gh/diraison/GenPyExo/external/processing/__init__.js',
                                dependencies: ['https://cdn.jsdelivr.net/gh/diraison/GenPyExo/external/deps/processing.js']
                            }
                        };

                        Sk.configure({
                            inputfun: function (prompt) {
                                return window.prompt(prompt);
                            },
                            inputfunTakesPrompt: true,
                            output: pythonout(elt),
                            read: builtinRead,
                            __future__: SkFuture,
                            killableWhile: true,
                            killableFor: true,
                            //inputfun : null , // fonction d'entrée personnalisée, voir https://github.com/skulpt/skulpt/issues/685
                            //inputfunTakesPrompt:true
                        });
                        (Sk.TurtleGraphics || (Sk.TurtleGraphics = {})).target = elt._graphicdiv;
                        var myPromise = Sk.misceval.asyncToPromise(function () {
                            return Sk.importMainWithBody("<stdin>", false, prog, true);
                        });
                        myPromise.then(function (mod) {
                            console.log('Python évalué avec succés');
                            elt._graphicdiv.style = ""; //pyplot block style incompatible with bootstrap tabs
                        },
                            function (err) {
                                console.log(err.toString());
                                console.log(document.getElementById('errorModal'));
                                let errorBody = document.querySelector('#errorModal .modal-body');
                                errorBody.innerHTML = "<pre>" + err.toString() + "</pre>";
                                //document.querySelector('#errorModal').modal('show');
                            });
                    }(elt, elt.editor.getValue(), elt._pythonoutput));
                };
            }

            function mySave(e) {
                return function () {
                    (function (elt) {
                        if (elt._restoreButton.classList.contains('disabled')) {
                            elt._restoreButton.classList.remove('disabled');
                        }
                        elt.datas.savedPython = elt.editor.getValue();
                    }(e));
                };
            }

            function myRestore(elt) {
                return function () {
                    (function (elt) {
                        elt.editor.setValue(elt.datas.savedPython, 1);
                    }(elt));
                };
            }

            function myReload(elt) {
                return function () {
                    (function (elt) {
                        elt.editor.setValue(elt.datas.initialPython, 1);
                    }(elt));
                };
            }

            function myDownload(elt) {
                return function () {
                    (function (elt) {
                        var blob = new Blob([elt.editor.getValue()], {
                            type: "text/x-python;charset=utf-8"
                        });
                        saveAs(blob, "programme.py");
                    }(elt));
                };
            }

            //this.acediv.style.minHeight = "20rem";
            //this.editor = ace.edit(this.acediv);
            //this.editor.setTheme("ace/theme/monokai");
            //this.editor.session.setMode("ace/mode/python");
            this._runButton.onclick = myRun(this);
            this._saveButton.onclick = mySave(this);
            this._restoreButton.onclick = myRestore(this);
            this._reloadButton.onclick = myReload(this);
            this._downloadButton.onclick = myDownload(this);

            this.loadPython();
        }

        // Load Python 
        loadPython() {
            const python = this.getAttribute('data-python');
            if (python != "" && python != null) {
                var httpRequest = new XMLHttpRequest();
                httpRequest.onreadystatechange = (function (elt, xhr) {
                    return function (data) {
                        var DONE = 4; // readyState 4 means the request is done.
                        var OK = 200; // status 200 is a successful return.
                        if (xhr.readyState === DONE) {
                            if (xhr.status === OK) {
                                elt.datas.initialPython = xhr.responseText;
                                elt.editor.setValue(elt.datas.initialPython, 1);
                            }
                        }
                    };
                }(this, httpRequest));
                httpRequest.open("GET", python);
                httpRequest.send();
            } else {
                this.editor.setValue(this.datas.initialPython, 1);
            }
            if (this.hasAttribute('graphic-first')) {
                this.pythondiv.classList.remove('show', 'active');
                this.graphicdiv.classList.add('show', 'active');
                this.consoleTab.classList.remove('active');
                this.graphicTab.classList.add('active');
            }

            if (this.hasAttribute('small-editor')) {
                this.pythondiv.classList.add('pyexec-small');
                this.graphicdiv.classList.add('pyexec-small');
                this.acediv.classList.add('pyexec-small');
            }

            if (this.hasAttribute('fixed-result')) {
                this.pythondiv.classList.add('pyexec-fixed');
            }

        }

        dec2hex(dec) {
            return ('0' + dec.toString(16)).substring(0, 2);
        }

        // generateId :: Integer -> String
        generateId(len) {
            var arr = new Uint8Array(len / 2);
            window.crypto.getRandomValues(arr);
            return Array.from(arr, this.dec2hex).join('');
        }
    }

    that.transformedDiv = {};

    that.init = function () {
        console.log('all loading done');
        /* Loading templates */
        fetch("templates/blocs.html")
            .then(response => response.text())
            .then(function (data) {
                let template = document.createElement('templates');
                template.innerHTML = data;
                document.getElementsByTagName('body')[0].prepend(template);
            })
            .then(function () {
                let md = new MD();
                md.inline_convert_all();
                let customElementRegistry = window.customElements;
                customElementRegistry.define('bloc-cours', Cours);

                customElementRegistry.define('bloc-attention', Attention);
                customElementRegistry.define('bloc-retenir', Retenir);
                customElementRegistry.define('bloc-todo', Todo);
                customElementRegistry.define('bloc-info', Info);
                customElementRegistry.define('bloc-exemple', Exemple);

                customElementRegistry.define('bloc-afaire', Afaire);
                customElementRegistry.define('bloc-consigne', Consigne);
                customElementRegistry.define('bloc-correction', Correction);
                customElementRegistry.define('bloc-remarque', Remarque);
                customElementRegistry.define('bloc-resume', Resume);
                customElementRegistry.define('bloc-abstract', Abstract);

                customElementRegistry.define('bloc-tip', Tip);
                customElementRegistry.define('bloc-succes', Succes);
                customElementRegistry.define('bloc-echec', Echec);
                customElementRegistry.define('bloc-danger', Danger);
                customElementRegistry.define('bloc-bug', Bug);

                customElementRegistry.define('bloc-citation', Citation);
                customElementRegistry.define('bloc-code', Code);

                customElementRegistry.define('bloc-python', Python);
            });
    };

    that.parseFile = function () {

    };

    return that;
})();

switch (document.readyState) {
    case "loading":
        // Encore en chargement.
        window.addEventListener("DOMContentLoaded", (event) => educHelper.init());
        break;
    case "interactive":
    case "complete":
        educHelper.init();
}