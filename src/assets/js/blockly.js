/* jshint browser: true, esversion: 10  */

window.blockly = (function () {
    "use strict";
    let that = {};
    // BLOCKLY
    that.init = function () {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                that.create(this.responseText);
            }
        };
        xhttp.open("GET", 'assets/toolboxes/defaultToolbox.xml', true);
        xhttp.send();
    };

    that.create = function (loadedToolbox) {
        window.blocklyTab = new bootstrap.Tab(document.querySelector("#blockly-tab"));
        window.blocklyTab.show();

        var blocklyDiv = document.getElementById('blocklyDiv');
        that.workspace = Blockly.inject(blocklyDiv,
            { toolbox: loadedToolbox });
        window.addEventListener('resize', that.onresize, false);
        that.onresize();
        Blockly.svgResize(that.workspace);
        // Workaround for blockly in bootstrap modal
        $('.blocklyWidgetDiv').prependTo('.modal');
        $('.blocklyTooltipDiv').prependTo('.modal');
        $('.blocklyDropDownDiv').prependTo('.modal');
        // resize on shown
        document.querySelector('#blockly-tab').addEventListener('shown.bs.tab', that.onresize);
    };

    that.onresize = function (e) {
        // Compute the absolute coordinates and dimensions of blocklyArea.
        var element = document.getElementById('blocklyArea');
        var x = 0;
        var y = 0;
        do {
            x += element.offsetLeft;
            y += element.offsetTop;
            element = element.offsetParent;
        } while (element);
        // Position blocklyDiv over blocklyArea.
        blocklyDiv.style.left = x + 'px';
        blocklyDiv.style.top = y + 'px';
        blocklyDiv.style.width = blocklyArea.offsetWidth + 'px';
        blocklyDiv.style.height = blocklyArea.offsetHeight + 'px';
        Blockly.svgResize(that.workspace);
    };
    return that;
})();

