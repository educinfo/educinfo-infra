/* jshint browser: true, esversion:10, sub:true */
/* globals bootstrap,notie,Basthon,blockly,Blockly,console,editor,shell,graphics */
/**
 * @namespace gui
 */
function makeGUI() {
    "use strict";
    let that = {};
    that.is_launched = false;

    that.modal = document.getElementById("pythonModal");
    that.bsModal = new bootstrap.Modal(that.modal);
    /**
     * @memberof gui
     * @function
     * @name notifyError
     * @param {string} error - le message d'erreur
     * @description Affiche le message d'erreur en utilisant noTie
     */
    that.notifyError = function (error) {
        that._console_error(error);
        const message = error.message || error.reason.message || error;
        notie.alert({
            type: 'error',
            text: 'Erreur : ' + message,
            stay: false,
            time: 3,
            position: 'top',
        });
        // In case of error, force loader hiding.
        try {
            // Basthon.Goodies.hideLoader();
        } catch { }
    };

    /**
     * @memberof gui
     * @function
     * @name init
     * @description Initialise l'interface graphique en initialisant tous
     * les composants liés à l'interpréteur Python et à l'édition
     */
    that.init = async function () {
        /* all errors redirected to notification system */
        const onerror = that.notifyError;
        window.addEventListener('error', onerror);
        window.addEventListener("unhandledrejection", onerror);
        /* console errors redirected to notification system */
        that._console_error = console.error;
        console.error = function () {
            onerror({ message: String(arguments[0]) });
        };

        /* state variables */
        that.initStateFromStorage('switched', false);
        that.initStateFromStorage('darkmode', true);

        /* update appearance from state variables */
        that.updateSwitchView();
        that.updateDarkLight();

        /* connecting buttons */
        // run
        let button = document.getElementById('run');
        button.addEventListener('click', that.runScript);
        // open
        button = document.getElementById("open");
        button.addEventListener("click", that.openFile);
        // download
        button = document.getElementById("download");
        button.addEventListener("click", that.download);

        // reload
        button = document.getElementById('reload');
        button.addEventListener('click', that.reload);

        // raz
        button = document.getElementById('raz');
        button.addEventListener('click', that.restart);

        // dark/light mode
        button = document.getElementById("darklight");
        button.addEventListener("click", that.switchDarkLight);

        // Fullscreen mode 
        button = document.getElementById("fullscreen");
        button.addEventListener("click", that.switchFullScreen);

        button = document.getElementById("toCodeButton");
        button.addEventListener("click", that.codeCreate);


        let tab = document.getElementById('blockly-tab');
        tab.addEventListener('shown.bs.tab', that.blocklyTabShown);
        tab.addEventListener('hidden.bs.tab', that.blocklyTabHidden);

        tab = document.getElementById('editor-tab');
        tab.addEventListener('shown.bs.tab', that.editorTabShown);
        tab.addEventListener('hidden.bs.tab', that.editorTabHidden);

        that.modal.addEventListener('shown.bs.modal', window.blockly.onresize);
        that.modal.addEventListener('hide.bs.modal', that.memorizeActiveTab);
        // initialise Basthon
        await that.basthonInit();

        that.sleep = function (ms) {
            return new Promise(
                resolve => setTimeout(resolve, ms)
            );
        };

        Basthon.inputAsync = async (prompt) => {
            console.log("prompt");
            await that.sleep(100);
            document.querySelector('#shell .shellInterior').innerHTML += "<span style=\"color:#55c\">" + prompt + "</span>";
            let result = window.prompt(prompt);
            document.querySelector('#shell .shellInterior').innerHTML += "<span style=\"color:#55c\">" + result + "</span><br>";
            return result;
        };
    };

    /**
     * @memberof gui
     * @function
     * @name switchFullScreen
     * @description Permet de faire basculer le modal Python en plein écran 
     * ou en modal normal
     */
    that.switchFullScreen = function () {
        let element = document.querySelector("#pythonModal .modal-dialog");

        if (element.classList) {
            element.classList.toggle("modal-fullscreen");
            blockly.onresize();
        }
    };

    /**
     * @memberof gui
     * @function 
     * @name editorTabShown
     * @param {*} e 
     * @description Callback appelé quand l'éditeur est affiché.
     * Met à jour correctement le contenu de l'éditeur et actualise 
     * les boutons.
     */
    that.editorTabShown = function (e) {
        // Update editor, because it can't be done in background
        window.editor.renderer.updateFull(true);

        let to_hide = {
            'run': "", 'open': "", 'download': "", 'reload': "",
            'raz': "none"
        };
        let button = null;
        for (var key in to_hide) {
            button = document.getElementById(key);
            button.style.display = to_hide[key];
        }
    };

    /**
     * @memberof gui
     * @function 
     * @name editorTabHidden
     * @param {*} e 
     * @description Callback appelé quand l'éditeur est caché.
     *  Actualise les boutons.
     */
    that.editorTabHidden = function (e) {
        let button = document.getElementById("run");
        button.style.display = "none";
        button = document.getElementById("open");
        button.style.display = "none";
        button = document.getElementById("download");
        button.style.display = "none";
        button = document.getElementById("reload");
        button.style.display = "none";
        button = document.getElementById("raz");
        button.style.display = "";
    };

    /**
     * Show compile button when blockly appears
     */
    that.blocklyTabShown = function () {
        let button = document.getElementById("toCodeButton");
        button.style.display = "";
    };

    /**
     * Hide compiles button when blockly is gone
     */
    that.blocklyTabHidden = function () {
        let button = document.getElementById("toCodeButton");
        button.style.display = "none";
    };

    /**
     *  Transform blockly blocks  into Python code
     *  Then open editor to see result. 
     */

    that.codeCreate = function () {
        var source = Blockly['Python'].workspaceToCode(window.blockly.workspace);
        var tab = new bootstrap.Tab(document.querySelector("#editor-tab"));
        tab.show();
        editor.setValue(source, -1);
    };


    /**
     * Change loader text and call init function.
     * In case of error, we continue the init process,
     * trying to do our best...
     */
    that.initCaller = async function (func, message) {
        // Basthon.Goodies.setLoaderText(message);
        try {
            return await func();
        } catch (error) { that.notifyError(error); }
    };

    /**
     * Loading Basthon and connecting to events.
     */
    that.basthonInit = async function () {
        // loading Basthon (errors are fatal)
        // await Basthon.Goodies.showLoader("Chargement de Basthon...", false);
        const init = that.initCaller;
        // loading blockly 
        await init(blockly.init, "Chargement de Blockly...");
        // loading editor
        await init(editor.init, "Chargement de l'éditeur...");
        // loading shell
        await init(shell.init, "Chargement de la console...");

        // loading graphics
        await init(graphics.init, "Chargement de la sortie graphique...");
        // loading aux files from URL
        await init(that.loadURLAux, "Chargement des fichiers auxiliaires...");
        // loading modules from URL
        await init(that.loadURLModules, "Chargement des modules annexes...");
        // end
        //Basthon.Goodies.hideLoader();

        /* backup before closing */
        window.onbeforeunload = function () {
            editor.backup().catch(that.notifyError);
        };
    };

    /**
     * Load ressources from URL (common part to files and modules).
     */

    that._loadFromURL = async function (key, put) {
        const url = new URL(window.location.href);
        let promises = [];
        const putFile = function (filename, data) {
            return put(filename, data);
        };
        for (let fileURL of url.searchParams.getAll(key)) {
            fileURL = decodeURIComponent(fileURL);
            const filename = fileURL.split('/').pop();
            let promise = Basthon.xhr({
                method: "GET",
                url: fileURL,
                responseType: "arraybuffer"
            });
            promise = promise.then(data => putFile(filename, data))
                .catch(function () {
                    throw {
                        message: "Impossible de charger le fichier " + filename + ".",
                        name: "LoadingException"
                    };
                });
            promises.push(promise);
        }
        await Promise.all(promises);
    };

    /**
     * Load auxiliary files submited via URL (aux= parameter) (async).
     */
    that.loadURLAux = function () {
        return that._loadFromURL('aux', Basthon.putFile.bind(Basthon));
    };

    /**
     * Load modules submited via URL (module= parameter) (async).
     */
    that.loadURLModules = function () {
        return that._loadFromURL('module', Basthon.putModule.bind(Basthon));
    };

    /**
     * Init a state variable from local storage (with default value).
     */
    that.initStateFromStorage = function (state, _default = false) {
        if (typeof (Storage) !== "undefined" && localStorage.getItem(state) !== null) {
            that[state] = localStorage.getItem(state) === "true";
        } else {
            that[state] = _default;
        }
    };

    /**
     * Save a state variable in local storage.
     */
    that.saveStateInStorage = function (state) {
        if (typeof (Storage) !== "undefined") {
            localStorage.setItem(state, that[state]);
        }
    };

    /**
     * Run the editor script in the shell.
     */
    that.runScript = function () {
        // backup the script just before running
        editor.backup().catch(that.notifyError);
        const src = editor.getValue();
        if (that.hasShell) {
            let tab = new bootstrap.Tab(document.getElementById("shell-tab"));
            tab.show();
        } else if (that.hasGraphics) {
            let tab = new bootstrap.Tab(document.getElementById("graphics-tab"));
            tab.show();
        }
        that.restart();
        shell.launch(src, false);

    };

    /**
     * RAZ function.
     */
    that.restart = function () {
        if (document.querySelector("#pythonModal .alert") != null) {
            let alertNode = document.querySelector("#pythonModal .alert");
            let bsAlert = new bootstrap.Alert(alertNode);
            bsAlert.close();
        }
        Basthon.restart();
        shell.clear();
        graphics.clean();
    };

    /**
     * Loading file in the (emulated) local filesystem (async).
     */
    that.putFSRessource = function (file) {
        return new Promise(function (resolve, reject) {
            const reader = new FileReader();
            reader.readAsArrayBuffer(file);
            reader.onload = async function (event) {
                await Basthon.putRessource(file.name, event.target.result);
                notie.alert({
                    type: 'success',
                    text: file.name + " est maintenant utilisable depuis Python",
                    stay: false,
                    time: 3,
                    position: 'top'
                });
                resolve();
            };
            reader.onerror = reject;
        });
    };

    /**
     * Open file in editor.
     */
    that.openEditor = async function (file) {
        await editor.open(file);
        notie.alert({
            type: 'success',
            text: file.name + " est chargé dans l'éditeur",
            stay: false,
            time: 3,
            position: 'top'
        });
    };

    /**
     * Open *.py file by asking user what to do:
     * load in editor or put on (emulated) local filesystem.
     */
    that.openPythonFile = async function (file) {
        notie.confirm({
            text: "Que faire de " + file.name + " ?",
            submitText: "Charger dans l'éditeur",
            cancelText: "Installer le module",
            position: 'top',
            submitCallback: () => { that.openEditor(file); },
            cancelCallback: () => { that.putFSRessource(file); }
        });
    };

    /**
     * Opening file: If it has .py extension, loading it in the editor
     * or put on (emulated) local filesystem (user is asked to),
     * otherwise, loading it in the local filesystem.
     */
    that.openFile = function () {
        return new Promise(function (resolve, reject) {
            let input = document.createElement('input');
            input.type = 'file';
            input.style.display = "none";
            input.onchange = async function (event) {
                for (let file of event.target.files) {
                    const ext = file.name.split('.').pop();
                    if (ext === 'py') {
                        await that.openPythonFile(file);
                    } else {
                        await that.putFSRessource(file);
                    }
                }
                resolve();
            };
            input.onerror = reject;

            document.body.appendChild(input);
            input.click();
            document.body.removeChild(input);
        });
    };

    /**
     * Download script in editor.
     */
    that.download = function () {
        editor.download("script.py");
    };

    /**
     * reload initial script in editor.
     */
    that.reload = function () {
        window.editor.loadScript(window.gui.pythonId, window.pythonUrl, true);
        that.restart();
    };


    /**
     * Displaying editor alone.
     */
    that.hideConsole = function () {
        let button = document.getElementById("hide-console");
        button.style.display = "none";
        button = document.getElementById("hide-editor");
        button.style.display = "";
        let elt = document.getElementById("right-div");
        elt.style.display = "none";
        elt = document.getElementById("left-div");
        elt.style.display = "";
        elt.style.width = "100%";
    };

    /**
     * Displaying console alone.
     */
    that.hideEditor = function () {
        let button = document.getElementById("hide-editor");
        button.style.display = "none";
        button = document.getElementById("show-editor-console");
        button.style.display = "";
        let elt = document.getElementById("left-div");
        elt.style.display = "none";
        elt = document.getElementById("right-div");
        elt.style.display = "";
        elt.style.width = "100%";
    };

    /**
     * Editor and console side by side.
     */
    that.showEditorConsole = function () {
        let button = document.getElementById("show-editor-console");
        button.style.display = "none";
        button = document.getElementById("hide-console");
        button.style.display = "";
        let elt = document.getElementById("left-div");
        elt.style.display = "";
        elt.style.width = "50%";
        elt = document.getElementById("right-div");
        elt.style.display = "";
        elt.style.width = "48%";
    };

    /**
     * Update switch view.
     */
    that.updateSwitchView = function () {
        let div_left = document.getElementById("left-div");
        let div_right = document.getElementById("right-div");

        if (that.switched) {
            div_left.style.cssFloat = "right";
            div_right.style.cssFloat = "left";
        } else {
            div_left.style.cssFloat = "left";
            div_right.style.cssFloat = "right";
        }
    };

    /**
     * Switching side view.
     */
    that.switchView = function () {
        that.switched = !that.switched;
        that.updateSwitchView();
        that.saveStateInStorage('switched');
    };

    /**
     * Update dark/light appearence.
     */
    that.updateDarkLight = function () {
        const elements = document.getElementsByClassName("darklighted");
        const func = that.darkmode ? 'remove' : 'add';
        for (const e of elements) e.classList[func]("light");
        const theme = that.darkmode ? "tomorrow_night_bright" : "xcode";
        editor.setTheme(`ace/theme/${theme}`);
    };

    /**
     * Switch dark/light mode.
     */
    that.switchDarkLight = function () {
        that.darkmode = !that.darkmode;
        that.saveStateInStorage('darkmode');
        that.updateDarkLight();
    };

    /**
     * Get mode as a string (dark/light).
     */
    that.theme = function () {
        return that.darkmode ? "dark" : "light";
    };

    /**
     * Toggle hide/show shell/graph.
     */
    that.showShell = function () {
        shell.style.display = "block";
        graphics.style.display = "none";
    };

    /**
     * Toggle hide/show shell/graph.
     */
    that.showGraph = function () {
        shell.style.display = "none";
        graphics.style.display = "flex";
    };

    /**
     * Share script via URL.
     */
    that.share = function () {
        const url = editor.sharingURL();
        notie.confirm({
            text: document.getElementById("share_message").innerHTML,
            submitText: "Copier dans le presse-papier",
            cancelText: "Tester le lien",
            position: 'top',
            submitCallback: function () { that.copyToClipboard(url); },
            cancelCallback: function () {
                let anchor = document.createElement("a");
                anchor.href = url;
                anchor.target = "_blank";
                anchor.style.display = "none"; // just to be safe!
                document.body.appendChild(anchor);
                anchor.click();
                document.body.removeChild(anchor);
            }
        });
    };

    /**
     * Copy a text to clipboard.
     */
    that.copyToClipboard = function (text) {
        let textArea = document.createElement("textarea");

        // Precautions from https://stackoverflow.com/questions/400212/how-do-i-copy-to-the-clipboard-in-javascript

        // Place in top-left corner of screen regardless of scroll position.
        textArea.style.position = 'fixed';
        textArea.style.top = 0;
        textArea.style.left = 0;

        // Ensure it has a small width and height. Setting to 1px / 1em
        // doesn't work as this gives a negative w/h on some browsers.
        textArea.style.width = '2em';
        textArea.style.height = '2em';

        // We don't need padding, reducing the size if it does flash render.
        textArea.style.padding = 0;

        // Clean up any borders.
        textArea.style.border = 'none';
        textArea.style.outline = 'none';
        textArea.style.boxShadow = 'none';

        // Avoid flash of white box if rendered for any reason.
        textArea.style.background = 'transparent';


        textArea.value = text;

        document.body.appendChild(textArea);
        textArea.focus();
        textArea.select();

        try {
            let successful = document.execCommand('copy');
            let msg = successful ? 'successful' : 'unsuccessful';
            console.log('Copying text command was ' + msg);
        } catch (err) {
            console.log('Oops, unable to copy');
        }

        document.body.removeChild(textArea);
    };

    that.memorizeActiveTab = function () {
        let tab = document.querySelector('.nav-link.active');
        that.activeTab = tab.getAttribute('aria-controls');
    };

    that.openModal = async function (evt) {
        let button = evt.target;
        let currentId = "default";
        if ("pythonId" in button.dataset) {
            currentId = button.dataset.pythonId;
        }
        /* Modifications uniquement si on utilise un nouveau bouton */
        if (that.pythonId != currentId) {
            shell.clear();
            graphics.clean();
            that.pythonId = currentId;
            that.activeTab = 'editor';
            if ("pythonUrl" in button.dataset)
                window.pythonUrl = button.dataset.pythonUrl;
            else
                window.pythonUrl = "ressources/common/python/empty.py";
            /* On charge le fichier à partir du localStorage ou de l'Url */
            window.editor.loadScript(window.gui.pythonId, window.pythonUrl);

            if ("activeTab" in button.dataset) {
                switch (button.dataset.activeTab) {
                    case 'blockly':
                        that.activeTab = 'blockly';
                        break;
                    case 'editor':
                        that.activeTab = 'editor';
                        break;
                    case 'shell':
                        that.activeTab = 'shell';
                        break;
                    case 'graphics':
                        that.activeTab = 'graphics';
                        break;
                    case 'consigne':
                        that.activeTab = 'consigne';
                        break;
                    default:
                        that.activeTab = 'editor';
                }
            } else {
                that.activeTab = 'editor';
            }

            if ("consigneUrl" in button.dataset) {
                let xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function () {
                    if (this.readyState == 4 && this.status == 200) {
                        let ol = document.querySelector('#consigne > ol');
                        ol.innerHTML = this.responseText;
                    }
                };
                xhttp.open("GET", button.dataset.consigneUrl, true);
                xhttp.send();
            }
        }
        // Blockly 
        if (("hasBlockly" in button.dataset) && button.dataset.hasBlockly.toLowerCase() == "true") {
            document.getElementById('blockly-tab').style.display = '';
            document.getElementById('blockly').style.display = '';
            that.hasBlockly = true;
        } else {
            document.getElementById('blockly-tab').style.display = 'none';
            document.getElementById('blockly').style.display = 'none';
            that.hasBlockly = false;
        }
        // Shell 
        if (("hasShell" in button.dataset) && button.dataset.hasShell.toLowerCase() == "false") {
            document.getElementById('shell-tab').style.display = 'none';
            document.getElementById('shell').style.display = 'none';
            that.hasShell = false;
        } else {
            document.getElementById('shell-tab').style.display = '';
            document.getElementById('shell').style.display = '';
            that.hasShell = true;
        }
        // Graphics
        if (("hasGraphics" in button.dataset) && button.dataset.hasGraphics.toLowerCase() == "false") {
            document.getElementById('graphics-tab').style.display = 'none';
            document.getElementById('graphics').style.display = 'none';
            that.hasGraphics = false;
        } else {
            document.getElementById('graphics-tab').style.display = '';
            document.getElementById('graphics').style.display = '';
            that.hasGraphics = true;
        }
        // Consignes
        if ("consigneUrl" in button.dataset) {
            document.getElementById('consigne-tab').style.display = '';
            document.getElementById('consigne').style.display = '';
            that.hasConsigne = true;
        } else {
            document.getElementById('consigne-tab').style.display = 'none';
            document.getElementById('consigne').style.display = 'none';
            that.hasConsigne = false;
        }

        let m = document.getElementById('pythonModal');
        let bsm = new bootstrap.Modal(m);
        bsm.show();
        let tab = new bootstrap.Tab(document.getElementById(that.activeTab + "-tab"));
        tab.show();
    };

    /**
     * @memberof gui
     * @function
     * @name download_file
     * @param {string} file_name - name of file to download from pyodide FS
     * @description permet de télécharger un fichier qui est dans le FS de pyodide
     */
    that.download_file = async function (file_name) {
        let txt = pyodide._module.FS.readFile(file_name);
        const blob = new Blob([txt], { type: 'application/text' });
        let url = window.URL.createObjectURL(blob);
        let a = document.createElement("a");
        a.style = "display: none";
        a.href = url;
        a.target = "_blank";
        a.download = file_name;
        let python = document.getElementById('pythonModal');
        python.appendChild(a);
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove();
    };

    /**
     * @memberof gui
     * @function
     * @name propose_downloads
     * @param None
     * @description modify dropdown to propose downloads
     */
    that.propose_downloads = function () {
        let fichiers = pyodide._module.FS.readdir('.');
        let downloadables = Array();
        fichiers.forEach(element => {
            let mode = pyodide._module.FS.stat(element).mode;
            if (pyodide._module.FS.isFile(mode)) {
                downloadables.push(element);
            }
        });
        if (downloadables.length == 0) {
            let item = document.createElement("li");
            item.className = "dropdown-item";
            item.innerHTML = "Aucun fichier";
            let ul = document.getElementById("fileList");
            ul.innerHTML = "";
            ul.appendChild(item);
        } else {
            let ul = document.getElementById("fileList");
            ul.innerHTML = "";
            downloadables.forEach(fichier => {
                let item = document.createElement("li");
                item.className = "dropdown-item";
                item.innerHTML = fichier;
                item.onclick = function () {
                    window.gui.download_file(fichier);
                };
                ul.appendChild(item);
            });
        }
    };

    return that;
}