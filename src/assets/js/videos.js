/* jshint browser: true, esversion: 10  */


/**
 * @namespace video
 * @description Ajout  pour les vidéos de Youtube et dailymotion
 * <p>
 * Ajouter ce module transforme les élèment avec la classe youtube ou dailymotion 
 * en vignette à partir de la vignette du site et permet de faire un lien pour ouvrir
 * la vidéo dans une fenêtre modale. Aucune action n'est nécessaire. 
 * </p>
 */
document.addEventListener('DOMContentLoaded', function (e) {

    "use strict";

    let videos = document.getElementsByClassName("youtube");

    /* Fonction pour les liens vers Youtube */
    let youtubeAdjust = function (e) {
        let targetTitle = document.querySelector('#videoModal .modal-title');
        targetTitle.innerHTML = this.dataset.videoTitle;
        let ratioDiv = document.createElement("div");
        ratioDiv.setAttribute('class', 'ratio ratio-16x9');
        let youtubeFrame = document.createElement("iframe");
        youtubeFrame.setAttribute('src', 'https://www.youtube.com/embed/' + this.dataset.videoId + '?cc_load_policy=1&cc_lang_pref=fr&autoplay=1');
        youtubeFrame.setAttribute('allow', "accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture");
        youtubeFrame.setAttribute('allowfullscreen', '');
        ratioDiv.appendChild(youtubeFrame);
        let targetDiv = document.querySelector('#videoModal .modal-body');
        targetDiv.innerHTML = '';
        targetDiv.appendChild(ratioDiv);
        let videoModal = bootstrap.Modal.getInstance(document.getElementById('videoModal'));
        videoModal.show();
    };

    for (var i = 0; i < videos.length; i++) {
        var youtube = videos[i];

        // Based on the YouTube ID, we can easily find the thumbnail image
        var img = document.createElement("img");
        img.setAttribute("src", "https://i.ytimg.com/vi/" + youtube.dataset.videoId + "/hqdefault.jpg");
        img.setAttribute("class", "img img-fluid img-thumbnail");

        // Overlay the Play icon to make it look like a video player
        var circle = document.createElement("div");
        circle.setAttribute("class", "circle");
        if (youtube.tagName != 'A') {
            youtube.innerHTML = '';
            youtube.appendChild(img);
            youtube.appendChild(circle);
        }

        youtube.addEventListener('click', youtubeAdjust, true);
    }
});

/** 
 * Ajout pour les vidéo Dailymotion
 */
 document.addEventListener('DOMContentLoaded', function (e) {

    "use strict";

    let videos = document.getElementsByClassName("dailymotion");

    /* Fonction pour les liens vers dailymotion */
    let dailymotionAdjust = function (e) {
        let targetTitle = document.querySelector('#videoModal .modal-title');
        targetTitle.innerHTML = this.dataset.videoTitle;
        let ratioDiv = document.createElement("div");
        ratioDiv.setAttribute('class', 'ratio ratio-16x9');
        let dailymotionFrame = document.createElement("iframe");
        dailymotionFrame.setAttribute('src', 'https://www.dailymotion.com/embed/video/' + this.dataset.videoId + '?autoplay=1');
        dailymotionFrame.setAttribute('allow', "autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture");
        dailymotionFrame.setAttribute('allowfullscreen', '');
        ratioDiv.appendChild(dailymotionFrame);
        let targetDiv = document.querySelector('#videoModal .modal-body');
        targetDiv.innerHTML = '';
        targetDiv.appendChild(ratioDiv);
        let videoModal = bootstrap.Modal.getInstance(document.getElementById('videoModal'));
        videoModal.show();
    };

    for (var i = 0; i < videos.length; i++) {
        var dailymotion = videos[i];

        // Based on the dailymotion ID, we can easily find the thumbnail image
        var img = document.createElement("img");
        img.setAttribute("src", "https://www.dailymotion.com/thumbnail/640x360/video/" + dailymotion.dataset.videoId);
        img.setAttribute("class", "img img-fluid img-thumbnail");

        // Overlay the Play icon to make it look like a video player
        var circle = document.createElement("div");
        circle.setAttribute("class", "circle");
        if (dailymotion.tagName != 'A') {
            dailymotion.innerHTML = '';
            dailymotion.appendChild(img);
            dailymotion.appendChild(circle);
        }
        dailymotion.addEventListener('click', dailymotionAdjust, true);
    }
});

/** 
 * Ajout pour les vidéo ARTE-TV
 */
document.addEventListener('DOMContentLoaded', function (e) {

    "use strict";

    let videos = document.getElementsByClassName("arteTV");

    /* Fonction pour les liens vers arteTV */
    let arteTVAdjust = function (e) {
        let targetTitle = document.querySelector('#videoModal .modal-title');
        targetTitle.innerHTML = this.dataset.videoTitle;
        let ratioDiv = document.createElement("div");
        ratioDiv.setAttribute('class', 'ratio ratio-16x9');
        let arteTVFrame = document.createElement("iframe");
        arteTVFrame.setAttribute('src', 'https://www.arte.tv/embeds/fr/' + this.dataset.videoId + '?autoplay=true&mute=0');
        arteTVFrame.setAttribute('allowfullscreen', 'true');
        ratioDiv.appendChild(arteTVFrame);
        let targetDiv = document.querySelector('#videoModal .modal-body');
        targetDiv.innerHTML = '';
        targetDiv.appendChild(ratioDiv);
        let videoModal = bootstrap.Modal.getInstance(document.getElementById('videoModal'));
        videoModal.show();
    };

    for (var i = 0; i < videos.length; i++) {
        var arteTV = videos[i];

        // Based on the arteTV ID, we can easily find the thumbnail image
        var img = document.createElement("img");
        img.setAttribute("src",  arteTV.dataset.videoThumbnail);
        img.setAttribute("class", "img img-fluid img-thumbnail");

        // Overlay the Play icon to make it look like a video player
        var circle = document.createElement("div");
        circle.setAttribute("class", "circle");
        if (arteTV.tagName != 'A') {
            arteTV.innerHTML = '';
            arteTV.appendChild(img);
            arteTV.appendChild(circle);
        }
        arteTV.addEventListener('click', arteTVAdjust, true);
    }
});

/** 
 * Ajout pour les vidéo Apps Tube
 */
document.addEventListener('DOMContentLoaded', function (e) {

    "use strict";

    let videos = document.getElementsByClassName("appsTube");

    /* Fonction pour les liens vers sur apps Tube */
    let appsTubeAdjust = function (e) {
        let targetTitle = document.querySelector('#videoModal .modal-title');
        targetTitle.innerHTML = this.dataset.videoTitle;
        let ratioDiv = document.createElement("div");
        ratioDiv.setAttribute('class', 'ratio ratio-16x9');
        let appsTubeFrame = document.createElement("iframe");
        appsTubeFrame.setAttribute('src', 'https://tube-sciences-technologies.apps.education.fr/videos/embed/' + this.dataset.videoId + "?autoplay=1");
        appsTubeFrame.setAttribute('sandbox', "allow-same-origin allow-scripts allow-popups");
        appsTubeFrame.setAttribute('allowfullscreen', '');
        ratioDiv.appendChild(appsTubeFrame);
        let targetDiv = document.querySelector('#videoModal .modal-body');
        targetDiv.innerHTML = '';
        targetDiv.appendChild(ratioDiv);
        let videoModal = bootstrap.Modal.getInstance(document.getElementById('videoModal'));
        videoModal.show();
    };

    for (var i = 0; i < videos.length; i++) {
        var appsTube = videos[i];

        // Based on the appsTube ID, we can easily find the thumbnail image
        var img = document.createElement("img");
        img.setAttribute("src", "https://tube-sciences-technologies.apps.education.fr/lazy-static/previews/" + appsTube.dataset.videoThumbnail+".jpg");
        img.setAttribute("class", "img img-fluid img-thumbnail");
        img.setAttribute("style","width:100%;")

        // Overlay the Play icon to make it look like a video player
        var circle = document.createElement("div");
        circle.setAttribute("class", "circle");
        if (appsTube.tagName != 'A') {
            appsTube.innerHTML = '';
            appsTube.appendChild(img);
            appsTube.appendChild(circle);
        }
        appsTube.addEventListener('click', appsTubeAdjust, true);
    }
});

/**
 * Suppression des vidéos à la fermeture du modal
 */
document.getElementById('videoModal').addEventListener('hide.bs.modal', function (e) {
    let targetDiv = document.querySelector('#videoModal .modal-body');
    targetDiv.innerHTML = '';
});