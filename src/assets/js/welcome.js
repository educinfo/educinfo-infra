(function ($) {
    "use strict"; // Start of use strict

    // jQuery for page scrolling feature - requires jQuery Easing plugin
    $('a.page-scroll').bind('click', function (event) {
        var anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($(anchor.attr('href')).offset().top - 50)
        }, 1250, 'easeInOutExpo');
        event.preventDefault();
    });

    /*// Highlight the top nav as scrolling occurs
    var scrollSpy = new bootstrap.ScrollSpy(document.body, {
        target: '#navbar-main',
        offset: 100
      });
      */

    // Closes the Responsive Menu on Menu Item Click
    $('.navbar-collapse ul li a').click(function () {
        $('.navbar-toggle:visible').click();
    });

    $(window).on('scroll', function (event) {
        var scrollValue = $(window).scrollTop();
        if (scrollValue > 50) {
            $('.navbar').addClass('affix');
            $('.navbar').removeClass('affix-top');
        } else {
            $('.navbar').addClass('affix-top');
            $('.navbar').removeClass('affix');
        }
    });


})(jQuery); // End of use strict