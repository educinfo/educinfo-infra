/* jshint browser: true, esversion: 10  */

function makeShell() {
    "use strict";
    let that = document.getElementById("shell");
    that.init = async function () {
        that.innerHTML = '<pre class="shellInterior"></pre>';
    };

    /* Event connections */
    Basthon.addEventListener("eval.finished", function (data) {
        if (data.interactive && "result" in data) {
            that.echo(data.result["text/plain"] + '\n');
        }
        that.wakeup();
    });

    Basthon.addEventListener("eval.output", function (data) {
        switch (data.stream) {
            case "stdout":
                that.echo(data.content);
                break;
            case "stderr":
                that.error(data.content);
                break;
        }
    });

    Basthon.addEventListener("eval.error", function (data) {
        that.wakeup();
    });


    /**
     * Print a string in the shell.
     */
    that.echo = function (message) {
        document.querySelector('#shell .shellInterior').innerHTML += message;
        return true;
    };

    /**
     * Print error in shell (in red).
     */
    that.error = function (message) {
        // JQuery.terminal adds a trailing newline that is not easy
        // to remove with the current API. Doing our best here.
        function escapeHtml(unsafe) {
            return unsafe
                .replace(/&/g, "&amp;")
                .replace(/</g, "&lt;")
                .replace(/>/g, "&gt;")
                .replace(/"/g, "&quot;")
                .replace(/'/g, "&#039;");
        }

        let alertDiv = document.createElement("div");
        alertDiv.classList.add("alert", "alert-danger", "alert-dismissible", "fade", "show");
        let content = "<strong>Erreur Python</strong>\n";
        content += "<pre>\n" + escapeHtml(message) + "</pre>";
        content += '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
        alertDiv.innerHTML = content;

        document.querySelector("#pythonModal .modal-body").prepend(alertDiv);

        let tab = new bootstrap.Tab(document.getElementById("editor-tab"));
        tab.show();

        return true;
    };

    /**
     * Pause the shell execution (usefull to wait for event or promise).
     */
    that.pause = function () {
        if (that.term != null) {
            that.term.pause();
        }
    };

    /**
     * Resume the shell execution (see `pause`).
     */
    that.wakeup = function () {
        if (that.term != null) {
            that.term.resume();
        }
    };

    /**
     * Recover the shell as it was at start.
     */
    that.clear = function () {
        document.querySelector('#shell').innerHTML = '<pre class="shellInterior"></pre>';
    };

    /**
     * Launch code in shell.
     */
    that.launch = function (code, interactive = true) {
        that.pause();
        Basthon.dispatchEvent("eval.request", {
            code: code,
            interactive: interactive
        });
    };

    return that;
}