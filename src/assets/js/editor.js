/* jshint browser: true, esversion:10, sub:true */
/* globals gui,bootstrap,notie,Basthon,blockly,Blockly,console,editor,shell,graphics */
/**
 * @namespace editor
 * @description éditeur <a href="https://ace.c9.io/"> ACE </a> pour éditer les scripts Pythons.
 */

function makeEditor() {
    "use strict";
    let that = window.ace.edit("editor");

    that.xhr = function (params) {
        const xhr = new XMLHttpRequest();
        xhr.open(params.method, params.url, true);
        if( params.responseType != null )
            xhr.responseType = params.responseType;
        const promise = new Promise(function(resolve, reject) {
            xhr.onload = function() {
                if( xhr.status >= 200 && xhr.status < 300 ) {
                    resolve(xhr.response);
                } else {
                    reject(xhr);
                }
            };
            xhr.onerror = function () { reject(xhr); };
        });
        // headers
        if (params.headers) {
            Object.keys(params.headers).forEach(function (key) {
                xhr.setRequestHeader(key, params.headers[key]);
            });
        }
        // data
        let data = params.data;
        if (data && typeof data === 'object') {
            data = JSON.stringify(data);
        }
        xhr.send(data);
        return promise;
    };

    /**
     * @memberof editor
     * @function
     * @name loadScript
     * @param {string} script_url script URL 
     * @param {string} script_id script_id for name in local storage
     * @description Charge le script initial dans l'éditeur
     *  <ul><li> à partir du localStorage si disponible</li>
     *  <li> à partir du script par défaut sinon </li></ul>
     */
    that.loadScript = async function (script_id="", script_url="ressources/common/python/empty.py",reinit=false) {
        that.setContent("# Script Python en cours de chargement");
        if ((typeof (localStorage) !== "undefined") && (script_id + "_py_src" in localStorage) && !reinit ) {
            that.setContent(localStorage[script_id+"_py_src"]);
            that.renderer.updateFull(); 
        } else {
            console.log("chargement du fichier "+script_url);
            let script;
            try {
                script = await that.xhr({
                    url: script_url,
                    method: 'GET',
                    responseType:'text'
                });
            } catch {
                throw {
                    message: "Le chargement du script " + script_url + " a échoué.",
                    name: 'LoadingException'
                };
            }
            if (script) {
                that.setContent(script);
                that.renderer.updateFull();
            }
        }
        that.scrollToRow(0);
        that.gotoLine(0);
    };

    /**
     * @memberof editor
     * @function
     * @name init
     * @description Initialise l'éditeur ACE, en fixant les options et en initialisant les completions.
     */
    that.init = async function () {
        that.session.setMode("ace/mode/python");
        that.focus();

        that.setOptions({
            'enableLiveAutocompletion': true,
            'enableBasicAutocompletion': true,
            'highlightActiveLine': false,
            'highlightSelectedWord': true,
            'fontFamily': "Fira Code",
            'fontSize': '11pt',
        });

        // removing all completers and keeping track of local completer
        const localCompleter = that.completers[1];  // any hint to detect this dynamically?
        that.completers = [{
            getCompletions: function (editor, session, pos, prefix, callback) {
                // get local completions (from editor, not namespace)
                let locals = [];
                localCompleter.getCompletions(editor, session, pos, prefix, function (_, completions) { locals = completions; });
                // recover editor content up to position
                let lines = editor.getValue().split("\n");
                lines = lines.slice(0, pos.row + 1);
                lines[lines.length - 1] = lines[lines.length - 1].slice(0, pos.column);
                const src = lines.join("\n");
                // Basthon completion
                let completions = Basthon.complete(src);
                const start = completions[1];
                completions = completions[0];
                // removing prefix
                const basthon_prefix = src.slice(start, -prefix.length);
                completions = completions.map(c => c.slice(basthon_prefix.length));
                // union with local completions
                completions = new Set(completions);
                for (let c of locals) {
                    c = c.value;
                    if (!(completions.has(c) || completions.has(c + "(")))
                        completions.add(c);
                }

                // rendering
                callback(null, [...completions].map(function (c) {
                    return {
                        caption: c,
                        value: c,
                        //meta: "code"
                    };
                }));
            }
        }];

        await that.loadScript();
    };

    /**
     * @memberof editor
     * @function
     * @name download
     * @param {string} [filename="script.py"] - nom du fichier python 
     * @description télécharge le contenu de l'éditeur avec pour nom filename
     */
    that.download = function (filename = "script.py") {
        let code = that.getValue();
        code = code.replace(/\r\n|\r|\n/g, "\r\n"); // To retain the Line breaks.
        let blob = new Blob([code], { type: "text/plain" });
        let anchor = document.createElement("a");
        anchor.download = filename;
        anchor.href = window.URL.createObjectURL(blob);
        anchor.target = "_blank";
        anchor.style.display = "none"; // just to be safe!
        document.body.appendChild(anchor);
        anchor.click();
        document.body.removeChild(anchor);
    };

    /**
     * @memberof editor
     * @function
     * @name setContent
     * @param {string} content - le contenu à mettre dans l'éditeur
     * @description fixe le contenu de l'éditeur à content.
     */
    that.setContent = function (content) {
        that.setValue(content);
        that.scrollToRow(0);
        that.gotoLine(0);
    };

    /**
     * @memberof editor
     * @function
     * @name open
     * @param {string} file - le fichier à charger
     * @description charge le fichier de façon asynchrone dans l'éditeur.
     */
    that.open = function (file) {
        return new Promise(function (resolve, reject) {
            const reader = new FileReader();
            reader.readAsText(file);
            reader.onload = function (event) {
                that.setContent(event.target.result);
                resolve();
            };
            reader.onerror = reject;
        });
    };

    /**
     * @memberof editor
     * @function 
     * @name backup
     * @param {string} script_id - id of script to be saved. Default to ""
     * @description Sauvegarde le contenu de l'éditeur en localStorage 
     */
    that.backup = async function (script_id = "default") {
        console.log("Sauvegarde dans"+script_id+"_py_src");
        if (typeof (localStorage) !== "undefined") {
            localStorage[script_id + "_py_src"] = that.getValue();
        }
    };

    return that;
}
