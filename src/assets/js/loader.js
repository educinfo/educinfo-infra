
function loadCss(cssUrl) {
  var link = document.createElement("link");
  link.setAttribute("rel","stylesheet");
  link.setAttribute("type","text/css");
  link.setAttribute("href",cssUrl);
  document.getElementsByTagName("head")[0].appendChild(link);  
}

/* Style */

loadCss("https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.4.0/styles/vs2015.min.css");
loadCss("https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css");
loadCss("https://cdn.jsdelivr.net/gh/dreampulse/computer-modern-web-font@master/fonts.css");
loadCss("https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.18.1/styles/monokai.min.css");
loadCss("https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css");
/* remix icons */
loadCss("https://cdn.jsdelivr.net/npm/remixicon@2.5.0/fonts/remixicon.css");
//loadCss("assets/css/educ-helper.css");
//loadCss("assets/css/cours.css");
/* Javascript via LabJS */

$LAB.setGlobalDefaults({AlwaysPreserveOrder:true});
$LAB.script("https://polyfill.io/v3/polyfill.min.js?features=es6")
  .script("https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js")
  .script("https://cdn.jsdelivr.net/npm/file-saver@2.0.2/dist/FileSaver.min.js")
  .script("https://cdnjs.cloudflare.com/ajax/libs/showdown/2.1.0/showdown.min.js")
  .script("https://cdn.jsdelivr.net/gh/trinketapp/skulpt-dist@0.11.1.19/skulpt.min.js").wait()
  .script("https://cdn.jsdelivr.net/gh/trinketapp/skulpt-dist@0.11.1.19/skulpt-stdlib.js")
  .script("https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.4.0/highlight.min.js")
  .script("https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.4.0/languages/python.min.js")
  .script("https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js")
  .script("https://cdnjs.cloudflare.com/ajax/libs/ace/1.4.14/ace.min.js" )
  .script("https://cdnjs.cloudflare.com/ajax/libs/ace/1.4.14/mode-python.min.js" )
  .script("https://cdnjs.cloudflare.com/ajax/libs/ace/1.4.14/theme-monokai.min.js")
  .script("assets/js/educ-helper.js");