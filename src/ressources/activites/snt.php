<?php

// SECTIONS EN SNT 
register_section(
	'internet',
	array(
		'category' => 'SNT',
		'titre' => "Internet",
		'commentaire' => "Internet est le réseau sur lequel repose notre monde numérique. En 1958 est apparu le premier modem. 
		En 1971, 23 ordinateurs étaient reliés sur ARPANET. Et de nos jours, ce sont des milliards de terminaux qui sont reliés 
		par ce réseau des réseaux. Un réseau international, sans gouvernance étatique"
	)
);
register_section(
	'web',
	array(
		'category' => 'SNT',
		'titre' => "Le Web",
		'commentaire' => "Le web n'est qu'une partie d'internet. C'est un des nombreux services de l'internet. Mais de notre 
		point de vue d'utilisateur final, il serait facile de le confondre, tant sa place est prépondérante."
	)
);
register_section(
	'sociaux',
	array(
		'category' => 'SNT',
		'titre' => "Les réseaux sociaux",
		'commentaire' => "On estime à plus de 4 milliards le nombre de personnes qui sont sur des réseaux sociaux informatiques.
		Leur influence est colossale. Ils font et défont les réputations, orientent nos choix et peuvent aller jusqu'à infléchir des
		élections. Comment fonctionnent t'ils ? Pourquoi ont ils ce pouvoir ?"
	)
);
register_section(
	'donnees',
	array(
		'category' => 'SNT',
		'titre' => "Les données structurées et leur traitement.",
		'commentaire' => "L'informatique, c'est la science du traitement de l'information, c'est à dire des données.
		Comment peut on structurer des données pour pouvoir les exploiter facilement ?"
	)
);
register_section(
	'gps',
	array(
		'category' => 'SNT',
		'titre' => "Localisation, cartographie et mobilité",
		'commentaire' => "À l'aube du XXIème siècle, lorsque l'on souhaitait se rendre d'un point à un autre, on prévoyait son voyage en utilisant des cartes routières. 
		Chacun avait chez lui un plan de sa ville. Les GPS existaient, mais leur usage était limité. De nos jours, les outils de cartographie sont indissociables de nos mobilités."
	)
);
register_section(
	'connecte',
	array(
		'category' => 'SNT',
		'titre' => "Informatique embarquée et objets connectés",
		'commentaire' => "Dans les années 1960, qui disait informatique voulait dire énormes machines qui occupaient des pièces entières et demandaient des ressources énergétiques énormes.
		De nos jours, on trouve des ordinateurs embarqués dans des montres et tout un tas d'objets connectés."
	)
);
register_section(
	'photo',
	array(
		'category' => 'SNT',
		'titre' => "La photographie numérique",
		'commentaire' => "De nos jours, un simple smartphone suffit pour faire des photographies d'une qualité tout à fait satisfaisante. La photo numérique, apparue avec 
		le XXIème siècle, a révolutionné la photgraphie mais aussi notre rapport à l'image."
	)
);
register_section(
	'programmation',
	array(
		'category' => 'SNT',
		'titre' => "Programmation",
		'commentaire' => "La programmation est l'apprentissage d'un langage qui vous donne la capacité de contrôler les ordinateurs. D'après Drew, le créateur de Dropbox, c'est ce que 
		nous avons de plus proche d'un super pouvoir."
	)
);




// SNT 
register_activity('RTSInternet',array(
	'category'=>'SNT',
	'section'=>'internet',
	'titre' => "Internet, histoire d'une révolution" ,
	'auteur'=>'RTS',
	'level'=>'initiation',
	'youtubeId'=>'5kXKPCqRbRI',
	'commentaire'=>"De sa création en 1969 à ses projections futures, chronologie d'une invention qui a révolutionné la société. Comment cet outil idéal, s'affranchissant des frontières pour transmettre la connaissance, est-il devenu le plus important marché commercial de l'histoire ?",
	'prerequis'=>NULL
)
);

register_activity('ARTEInternet',array(
	'category'=>'SNT',
	'section'=>'internet',
	'titre' => "Cables sous-marins, la guerre invisible" ,
	'auteur'=>'Arte, le dessous des cartes',
	'level'=>'initiation',
	'youtubeId'=>'MzcKHQyDL5o',
	'commentaire'=>"Les câbles sous-marins constituent l'épinde dorsale de l'infrastructure d'internet. Méconnus du grand public, ils ont une importance géostratégique et économique cruciale.",
	'prerequis'=>NULL
)
);


register_activity('ARTEInternet2',array(
	'category'=>'SNT',
	'section'=>'internet',
	'titre' => "Cables sous-marins, l'autre guerre" ,
	'auteur'=>'Arte, le dessous des cartes',
	'level'=>'initiation',
	'youtubeId'=>'EGWYbyvTF2Q',
	'commentaire'=>"Les câbles sous marins sont des ressources cruciales. Sont ils menacés en temps de guerre ?",
	'prerequis'=>NULL
)
);

register_activity('ARTEContreInternet',array(
	'category'=>'SNT',
	'section'=>'internet',
	'titre' => "Une contre histoire de l'internet" ,
	'auteur'=>'Arte',
	'youtubeId'=>'RKsNxRNV1j8',
	'commentaire'=>"Internet a été créé par des hippies tout en étant financé par des militaires ! Cet improbable choc des cultures a donné naissance à un espace de libertés impossible à censurer ou à contrôler. C'est pourtant ce que cherchent à faire, depuis des années, un certain nombre de responsables politiques, poussant hackers et défenseurs des libertés à entrer dans l'arène politique.",
	'prerequis'=>NULL
)
);

register_activity('MOOCIntroInternet',array(
	'category'=>'SNT',
	'section'=>'internet',
	'titre' => "Internet, IP un protocole universel ?" ,
	'auteur'=>'Pixees',
	'youtubeId'=>'aX3z3JoVEdE',
	'commentaire'=>"Qu'est ce que l'internet ? Cette vidéo introduit la notion dans le MOOC Fun",
	'prerequis'=>NULL
)
);

register_activity('InternetVSWeb',array(
	'category'=>'SNT',
	'section'=>'internet',
	'type'=>'url',
	'URL'=>'https://www.chosesasavoir.com/difference-entre-internet-web/',
	'titre' => "Internet et le web" ,
	'auteur'=>'Choses à savoir',
	'level'=>'initiation',
	'commentaire'=>"Quelle est la différence entre internet et le web ?",
	'prerequis'=>NULL,
	'icon'=>'fa fa-podcast'
)
);

register_activity('InternetGachis',array(
	'category'=>'SNT',
	'section'=>'internet',
	'type'=>'url',
	'URL'=>'https://lejournal.cnrs.fr/articles/numerique-le-grand-gachis-energetique',
	'titre' => "Numérique, le grand gâchis énergétique" ,
	'auteur'=>'Laure CAILLOCE - CNRS',
	'commentaire'=>"Ordinateurs, data centers, réseaux… engloutissent près de 10 % de la consommation mondiale d’électricité. Et ce chiffre ne cesse d’augmenter.",
	'prerequis'=>NULL,
	'icon'=>'far fa-newspaper'
)
);

// SNT : le web

register_activity('WEBMoocFun',array(
	'category'=>'SNT',
	'section'=>'web',
	'titre' => "Site internet ou site web ?" ,
	'auteur'=>'Pixees',
	'youtubeId'=>'GqD6AiaRo3U',
	'commentaire'=>"On entend parler aussi bien de site web que de site internet. Mais qu'en est il ?",
	'prerequis'=>NULL
)
);

register_activity('HistoireWebCanada',array(
	'category'=>'SNT',
	'section'=>'web',
	'titre' => "Le web a 30 ans : retour sur son histoire" ,
	'auteur'=>'Radio Canada Info',
	'youtubeId'=>'AIIVHMvpN6A',
	'commentaire'=>"Tim Berners-Lee a créé le web il y a 30 ans. Cet outil a révolutionné nos vies. Quels sont ses réussites… et ses échecs? Le reportage de Mathieu Prost.",
	'prerequis'=>NULL
)
);

register_activity('TBLWeb',array(
	'category'=>'SNT',
	'section'=>'web',
	'titre' => "Tim Berners-Lee, le génie inventeur du web" ,
	'auteur'=>'France Culture',
	'youtubeId'=>'PbCG_BeL5dY',
	'commentaire'=>"Le 12 mars 2019, le World Wide Web fête ses 30 ans. Derrière cette révolution technologique, un homme discret : Tim Berners-Lee. Défenseur d’un web libre et ouvert, il œuvre aujourd’hui contre la perte de contrôle des données personnelles. Portrait.",
	'prerequis'=>NULL
)
);

register_activity('OCRHtmlCss',array(
	'category'=>'SNT',
	'section'=>'web',
	'type'=>'url',
	'URL'=>'https://openclassrooms.com/fr/courses/1603881-apprenez-a-creer-votre-site-web-avec-html5-et-css3',
	'titre' => "Apprenez à créer votre site web avec HTML5 et CSS3" ,
	'auteur'=>'Mathieu NEBRA',
	'image'=>'assets/img/html5-et-css3.png',
	'commentaire'=>"Vous souhaitez créer vos propres sites web ? C'est le bon endroit ! Dans ce cours, vous apprendrez comment utiliser HTML5 et CSS3, les deux langages à la base de tous les sites web.",
	'prerequis'=>NULL
)
);

register_activity('GrafikartHTML',array(
	'category'=>'SNT',
	'section'=>'web',
	'type'=>'url',
	'URL'=>'https://grafikart.fr/cursus',
	'titre' => "Cursus de développement web." ,
	'auteur'=>'grafikart',
	'image'=>'assets/img/html5-et-css3.png',
	'commentaire'=>"Sur le site de Grafikart, vous trouverez une formation très progressive, complète et accessible sur le langage HTML. Vous trouverez aussi une formation sur la mise en page CSS, et bien d'autres choses",
	'prerequis'=>NULL
)
);

// Réseaux sociaux

register_activity('Moocsociaux',array(
	'category'=>'SNT',
	'section'=>'sociaux',
	'titre' => "Réseaux sociaux, le monde est-il si petit ?" ,
	'auteur'=>'Pixees',
	'youtubeId'=>'nn1mIqW9oYQ',
'commentaire'=>"Une introduction au sujet des réseaux sociaux.",
	'prerequis'=>NULL
)
);

register_activity('DopamineArteSnapchat',array(
	'category'=>'SNT',
	'section'=>'sociaux',
	'titre' => "Pourquoi vous êtes accros à Snapchat" ,
	'auteur'=>'Arte - Programmes courts',
	'appsTubeId'=>'fbd94bbf-e701-4202-ac6c-90178cd1d19a',
	'appsTubeThumbnail'=>'c5eb18f3-ed1b-4a82-8e5c-92e43d23cfbd',
	'commentaire'=>"Snapchat est super fun et tu ne peux pas t'emêcher de checker les derniers filtres pour envoyer des snaps à tes amis.
		C'est normal, car l'appli est basée sur le DON, une interaction universelle, ayant un caractère obligatoire.
		Ainsi tu te sens toujours obligé(e) de répondre par un contre-don, envoyant des snaps à tes amis, même si tu n'as rien à dire.",
	'prerequis'=>NULL
)
);

register_activity('DatagueuleReseaux',array(
	'category'=>'SNT',
	'section'=>'sociaux',
	'titre' => "Réseaux sociaux : flux à lier " ,
	'auteur'=>'Arte - Programmes courts',
	'youtubeId'=>'G1_ryVCLWoc',
	'commentaire'=>"Ils sont attirants, attachants… Et rapidement indispensables. 
	Les réseaux sociaux nous appâtent à coup de likes et nous bercent dans leurs \"infinite scroll\". 
	Et rapidement leurs interfaces nous poussent à la consommation jusqu’à l’overdose. 
	Et ce n’est pas un hasard. Ils ont bâti leurs empires sur notre addiction à la dopamine.",
	'prerequis'=>NULL
)
);

register_activity('HistoriqueReseaux',array(
	'category'=>'SNT',
	'section'=>'sociaux',
	'titre' => "Les réseaux sociaux: les références historiques" ,
	'auteur'=>'Le Prof',
	'youtubeId'=>'dKZFv7GYyH4',
	'commentaire'=>"Un aperçu historique de l'apparition des réseaux sociaux et des différentes évolutions et notions.",
	'prerequis'=>NULL
)
);

register_activity('ControleReseaux',array(
	'category'=>'SNT',
	'section'=>'sociaux',
	'titre' => "Les réseaux Sociaux doivent ils être mieux contrôlés ?" ,
	'auteur'=>'28 minutes - Arte',
	'arteTVId'=>'115760-002-A',
	'arteTVthumbnail'=>'https://api-cdn.arte.tv/img/v2/image/ieKaE4bF2CzsGegdsj3fvm/940x530',
	'commentaire'=>"Les réseaux sociaux ne sont pas à l’origine des émeutes urbaines.
		Mais, pendant sept nuits, les émeutiers ont pu communiquer grâce à Messenger, 
		Snapchat ou encore Instagram. Accusés d’être des amplificateurs de crise, 
		le contrôle des réseaux sociaux doit-il être accru ? ",
	'prerequis'=>NULL
)
);

register_activity('HeroineReseaux',array(
	'category'=>'SNT',
	'section'=>'sociaux',
	'titre' => "Envoyé spécial. L'addiction aux écrans : \"héroïne numérique\"" ,
	'auteur'=>'Envoyé spécial du 18 janvier 2018',
	'youtubeId'=>'DyK4vxbAmwQ',
	'commentaire'=>"Aujourd’hui, les scientifiques en sont persuadés : les écrans sont dangereux pour nos enfants. 
	Ils agissent sur leur cerveau, leur concentration. En France, des médecins lancent l’alerte. 
	Aux Etats-Unis, d’anciens salariés racontent comment les industriels entretiennent l’addiction aux jeux sur smartphone, 
	et des scientifiques dénoncent leur impact sur le développement de l’enfant. Que se passe-t-il réellement face aux écrans ?
	</p><p class=\"card-text\">
	Rayan a 3 ans. Pendant de longs mois, sa maman s’inquiète : toujours dans sa bulle, il ne parle plus et multiplie les crises de nerf. 
	Un jour, un médecin conseille à cette maman ne plus jamais laisser son enfant regarder de comptines sur son smartphone. 
	Rayan pouvait y passer des heures ! Au bout de quelques jours, sevré d’écran, Rayan redit \"maman\" pour la première fois depuis un an. 
	Peu à peu, il redevient un petit garçon ouvert et joyeux.",
	'prerequis'=>NULL
)
);


register_activity('DessousCartesReseaux',array(
	'category'=>'SNT',
	'section'=>'sociaux',
	'titre' => "Géopolitique des réseaux sociaux" ,
	'auteur'=>'Le dessous des cartes - Arte',
	'youtubeId'=>'hWeksJQQRyU',
	'commentaire'=>"
	L'ingérence des Gafam dans la dernière campagne électorale a marqué un tournant dans l’histoire des réseaux sociaux, 
	soulignant en quoi ces derniers jouaient désormais un rôle politique. 
	Évincé des grands réseaux sociaux depuis l’assaut meurtrier du Capitole en 
	janvier 2021 – au cours duquel certains ont cru voir la démocratie américaine vaciller –,
	l’ancien président américain Donald Trump a porté plainte contre Facebook, Twitter et Google. 
	Cette ingérence des géants américains du numérique dans une campagne électorale a marqué un tournant 
	dans l’histoire des réseaux sociaux, rappelant en quoi ces derniers jouaient désormais, de fait, un rôle politique. 
	Nous avons voulu dès lors rappeler en quoi les démocraties et les États autoritaires 
	n’en faisaient pas le même usage, en nous arrêtant sur le modèle chinois, où l’accès à 
	l’internet mondial n’est pas possible, et où les réseaux sociaux sont au cœur de la stratégie 
	communiste d’un contrôle étroit de la population.
	Mais également sur le modèle de l’État libéral américain, 
	qui en donnant carte blanche aux Gafam sans chercher à les réguler ni 
	encadrer la question cruciale de l’utilisation des données personnelles, s’est retrouvé dépassé. 
	En regardant enfin comment l’Europe est à la pointe de la réflexion règlementaire, sans avoir su développer sa propre puissance numérique.",
	'prerequis'=>NULL
)
);

register_activity('HexagoneReseaux',array(
	'category'=>'SNT',
	'section'=>'sociaux',
	'titre' => "ADDICTS AUX RÉSEAUX SOCIAUX vs DÉCONNECTÉS" ,
	'auteur'=>'HugoDécrypte - Actus du jour',
	'youtubeId'=>'RaAmat4xjaw',
	'commentaire'=>"
	Passe-t-on trop de temps sur notre téléphone ?
	 Nouvel épisode d'Hexagone, des jeunes qui passent des heures sur les réseaux sociaux face à des déconnectés ! ",
	'prerequis'=>NULL
)
);

register_activity('Sixdegres',array(
	'category'=>'SNT',
	'section'=>'sociaux',
	'type'=>'url',
	'titre' => "6 degrés de séparation? Peut-être même moins!" ,
	'auteur'=>'	Kathleen Couillard • Journaliste',
	'image'=>'assets/img/6_degres_separation-600x338.jpg',
	'URL'=>'https://l-express.ca/6-degres-de-separation-peut-etre-meme-moins/',
	'commentaire'=>"Il est bel et bien possible de connecter deux personnes 
	qui ne se connaissent pas, grâce à un nombre limité d’intermédiaires. 
	Les réseaux sociaux ont probablement diminué le nombre de personnes qui nous séparent les uns des autres.",
	'prerequis'=>NULL
)
);
// Données structurées

register_activity('MoocSNTData',array(
	'category'=>'SNT',
	'section'=>'donnees',
	'titre' => " Données, comment les manipuler ?" ,
	'auteur'=>'Pixees',
	'youtubeId'=>'IJJgcZ2DEs0',
	'commentaire'=>"Une introduction au sujet des données structurées.",
	'prerequis'=>NULL
)
);


// Localisation

register_activity('MoocSNTgps',array(
	'category'=>'SNT',
	'section'=>'gps',
	'titre' => " Géolocalisation, comment s'y retrouver ?" ,
	'auteur'=>'Pixees',
	'youtubeId'=>'iTfNhcC2vBA',
	'commentaire'=>"Une introduction au sujet de la cartographie numérique et de la géolocalisation.",
	'prerequis'=>NULL
)
);


// Informatique embarquée

register_activity('MoocSNTconnecte',array(
	'category'=>'SNT',
	'section'=>'connecte',
	'titre' => "Objets connectés, des robots dans nos maisons ?" ,
	'auteur'=>'Pixees',
	'youtubeId'=>'DOECi_ZKaYI',
	'commentaire'=>"Une introduction au sujet des objets connectés.",
	'prerequis'=>NULL
)
);


// photographie numérique

register_activity('MoocPhoto',array(
	'category'=>'SNT',
	'section'=>'photo',
	'titre' => " Photographie numérique, du réel aux pixels ?" ,
	'auteur'=>'Pixees',
	'youtubeId'=>'UnNPNc-F9ks',
	'commentaire'=>"Une introduction au sujet de la photographie numérique.",
	'prerequis'=>NULL
)
);

register_activity('HistoirePhoto',array(
	'category'=>'SNT',
	'section'=>'photo',
	'titre' => "Histoire de la PHOTOGRAPHIE - de l'Antiquité au Numérique" ,
	'auteur'=>'	Derrière la caméra',
	'youtubeId'=>'LHqE0B34fDU',
	'commentaire'=>"L'histoire de la photographie avec un grand H, de ses origines à aujourd'hui.",
	'prerequis'=>NULL
)
);

