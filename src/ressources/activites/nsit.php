<?php

register_section(
    'NSIThistoire',
    array(
        'category' => 'NSIT',
        'titre' => "Histoire de l'informatique",
        'commentaire' => "Comme toute connaissance scientifique et technique, les concepts de l’informatique ont une histoire et ont été forgés par des personnes. Les algorithmes sont présents dès l’Antiquité..."
    )
);

register_section(
    'NSITtypebase',
    array(
        'category' => 'NSIT',
        'titre' => "Représentation des données: types et valeurs de base",
        'commentaire' => "Toute machine informatique manipule une représentation des données dont l’unité minimale est le bit, ce qui permet d’unifier logique et calcul. Les données  de  base sont représentées selon un codage dépendant de leur nature:  entiers,  flottants, caractères et chaînes de caractères."
    )
);

register_section(
    'NSITtypeconstruit',
    array(
        'category' => 'NSIT',
        'titre' => "Représentation des données: types construits",
        'commentaire' => "À partir des types de base se constituent des types construits (p-uplets, tableaux, dictionnaires,etc.)"
    )
);

register_section(
    'NSITtables',
    array(
        'category' => 'NSIT',
        'titre' => "Traitement de données en tables",
        'commentaire' => "Les données organisées en table correspondent à une liste de p-uplets nommés qui partagent les mêmes descripteurs. La mobilisation de ce type de structure de données permet de préparer les élèves à aborder la notion de base de données qui ne sera présentée qu’en classe terminale."
    )
);

register_section(
    'NSITihm',
    array(
        'category' => 'NSIT',
        'titre' => "Interactions entre l’homme et la machine sur le Web",
        'commentaire' => "Lors de leur navigation sur le Web, les internautes interagissent avec leur machine par le biais des pages web. L’Interface Homme-Machine (IHM) repose sur la  gestion d’événements associés à des éléments graphiques munis de méthodes algorithmiques."
    )
);

register_section(
    'NSIThardos',
    array(
        'category' => 'NSIT',
        'titre' => "Architectures matérielles et systèmes d'exploitation",
        'commentaire' => "Cette partie du programme étudie l'architecture des machines ainsi que les systèmes d'exploitations qui permettent leur fonctionnement."
    )
);

register_section(
    'NSITlangages',
    array(
        'category' => 'NSIT',
        'titre' => "Langages et programmation",
        'commentaire' => "On étudie en première les constructions élémentaires des langages, en python, ainsi que les bases du javascript, des langages HTML et CSS"
    )
);

register_section(
    'NSITalgo',
    array(
        'category' => 'NSIT',
        'titre' => "Algorithmique",
        'commentaire' => "Le concept de méthode algorithmique est introduit en classe de première et quelques algorithmes classiques sont étudiés."
    )
);

