<?php
// Pages standards

register_section(
    'PROFSofficiel',
    array(
        'category' => 'PROFS',
        'titre' => "Ressources officielles ",
        'commentaire' => "Outre le programme officiel, le ministère propose aussi des ressources d'accompagnement pour l'enseignement de NSI et de SNT. On peut ajouter à cela les MOOC organisés sur la plateforme FUN (France Université Numérique) pour accompagner la préparation des enseignants à ces enseignements"
    )
);

register_section(
    'PROFSCours',
    array(
        'category' => 'PROFS',
        'titre' => "Cours de collègues en ligne",
        'commentaire' => "De nombreux collègues ont proposé des cours complets en ligne, souvent de très bonne qualité."
    )
);

register_section(
    'PROFSTransversal',
    array(
        'category' => 'PROFS',
        'titre' => "Ressources transversales",
        'commentaire' => "Des ressources sur les activités débranchées, la programmation, qui peuvent être utilisées de bien des façons."
    )
);

register_section(
    'PROFSOutils',
    array(
        'category' => 'PROFS',
        'titre' => "Outils",
        'commentaire' => "Différents outils utilisables dans l'enseigenment de l'informatique"
    )
);


// Officiel

register_activity(
	'programmeSNT',
	array(
		'category' => 'PROFS',
		'section' => 'PROFSofficiel',
		'type' => 'url',
		'titre' => "Le programme officiel de SNT en classe de seconde",
		'auteur' => 'Ministère de l\'éducation nationale',
		'URL' => 'https://cache.media.education.gouv.fr/file/SP1-MEN-22-1-2019/08/5/spe641_annexe_1063085.pdf',
		'commentaire' => "Programme officiel publié au BO du 22 janvier 2019",
		'image' => 'assets/img/logo-MEN.png',
		'prerequis' => NULL
	)
);

register_activity(
    'programmeNSI1',
    array(
        'category' => 'PROFS',
        'section' => 'PROFSofficiel',
        'type' => 'url',
        'titre' => "Le programme officiel de NSI en classe de première",
        'auteur' => 'Ministère de l\'éducation nationale',
        'URL' => 'https://cache.media.eduscol.education.fr/file/SP1-MEN-22-1-2019/26/8/spe633_annexe_1063268.pdf',
        'commentaire' => "Programme officiel publié au BO du 22 janvier 2019",
        'image' => 'assets/img/logo-MEN.png',
        'prerequis' => NULL
    )
);

register_activity(
    'programmeNSIT',
    array(
        'category' => 'PROFS',
        'section' => 'PROFSofficiel',
        'type' => 'url',
        'titre' => "Le programme officiel de NSI en classe de terminale",
        'auteur' => 'Ministère de l\'éducation nationale',
        'URL' => 'https://eduscol.education.fr/document/30010/download',
        'commentaire' => "Programme officiel publié au BO du 25 juillet 2019",
        'image' => 'assets/img/logo-MEN.png',
        'prerequis' => NULL
    )
);

register_activity(
	'accompagnementSNT',
	array(
		'category' => 'PROFS',
		'section' => 'PROFSofficiel',
		'type' => 'url',
		'titre' => "Page SNT sur EDUSCOL",
		'auteur' => 'Ministère de l\'éducation nationale',
		'URL' => 'https://eduscol.education.fr/1670/programmes-et-ressources-en-sciences-numeriques-et-technologie-voie-gt',
		'commentaire' => "L'ensemble des ressources officielles pour l'enseignement de SNT sur EDUSCOL",
		'image' => 'assets/img/eduscol-logo.jpg',
		'prerequis' => NULL
	)
);




register_activity(
    'ressources',
    array(
        'category' => 'PROFS',
        'section' => 'PROFSofficiel',
        'type' => 'url',
        'titre' => "Page NSI sur EDUSCOL",
        'auteur' => 'Ministère de l\'éducation nationale',
        'URL' => 'https://eduscol.education.fr/2068/programmes-et-ressources-en-numerique-et-sciences-informatiques-voie-gt',
        'commentaire' => "L'ensemble des ressources officielles pour l'enseignement de la spécialité NSI sur EDUSCOL",
        'image' => 'assets/img/eduscol-logo.jpg',
        'prerequis' => NULL
    )
);


register_activity(
	'moocNSI',
	array(
		'category' => 'PROFS',
		'section' => 'PROFSofficiel',
		'type' => 'url',
		'titre' => "MOOC FUN",
		'auteur' => 'France Université Numérique',
		'URL' => 'https://www.fun-mooc.fr/fr/cours/numerique-et-sciences-informatiques-les-fondamentaux/',
		'commentaire' => "FUN (France Université Numérique) propose un MOOC <em> Numérique et Sciences Informatiques, NSI : les fondamentaux </em> ",
		'image' => 'assets/img/funmooc.png',
		'prerequis' => NULL
	)
);

register_activity(
	'moocSNT',
	array(
		'category' => 'PROFS',
		'section' => 'PROFSofficiel',
		'type' => 'url',
		'titre' => "MOOC FUN",
		'auteur' => 'France Université Numérique',
		'URL' => 'https://www.fun-mooc.fr/courses/course-v1:inria+41018+session01/about',
		'commentaire' => "FUN (France Université Numérique) propose un MOOC <em> S'initer à l'enseignement en SNT </em> très utile pour les enseignants",
		'image' => 'assets/img/funmooc.png',
		'prerequis' => NULL
	)
);


// Cours de collègues

register_activity(
    'pixees',
    array(
        'category' => 'PROFS',
        'section' => 'PROFSCours',
        'type' => 'url',
        'titre' => "NSI sur Pixees",
        'auteur' => 'David ROCHE',
        'URL' => 'https://pixees.fr/informatiquelycee/',
        'commentaire' => "La dernière version des cours de SNT et de NSI de David ROCHE en ligne sur pixees.",
        'icon' => 'fa fa-globe',
        'prerequis' => NULL
    )
);

register_activity(
    'carnot',
    array(
        'category' => 'PROFS',
        'section' => 'PROFSCours',
        'type' => 'url',
        'titre' => "Maths-code",
        'auteur' => 'Romain DEBAILLEUL',
        'URL' => 'http://maths-code.fr/cours/',
        'commentaire' => "Cours de première et terminale NSI ainsi que plein d'autres choses.",
        'image' => 'assets/img/maths-code.png',
        'prerequis' => NULL
    )
);

register_activity(
    'MLN',
    array(
        'category' => 'PROFS',
        'section' => 'PROFSCours',
        'type' => 'url',
        'titre' => "Mon Lycée Numérique",
        'auteur' => 'Team MLN',
        'URL' => 'https://monlyceenumerique.fr/index.html',
        'commentaire' => "Un ensemble d'activités pour NSI, SNT mais aussi des mathématiques, du python, de l'enseignement scientifique, mis en ligne par un ensemble de professeurs dans l'académie de Reims.",
        'image' => 'assets/img/logo_mln.png',
        'prerequis' => NULL
    )
);

register_activity(
    'Lyceum',
    array(
        'category' => 'PROFS',
        'section' => 'PROFSCours',
        'type' => 'url',
        'titre' => "LYCEUM",
        'auteur' => 'Benjamin ABEL',
        'URL' => 'https://www.lyceum.fr/',
        'commentaire' => "Cours de SNT, NSI (1ère et terminale) mis en ligne par Benjamin ABEL",
        'icon' => 'fa fa-globe',
        'prerequis' => NULL
    )
);

register_activity(
    'Fleret',
    array(
        'category' => 'PROFS',
        'section' => 'PROFSCours',
        'type' => 'url',
        'titre' => "Informatique au lycée Faustin FLERET",
        'auteur' => 'Ludovic SOMMERFELD',
        'URL' => 'https://nsinfo.yo.fr',
        'commentaire' => "Cours de SNT, NSI (1ère et terminale) mis en ligne par Ludovic SOMMERFELD",
        'image' => 'assets/img/fleret.jpg',
        'prerequis' => NULL
    )
);

register_activity(
    'Mathex',
    array(
        'category' => 'PROFS',
        'section' => 'PROFSCours',
        'type' => 'url',
        'titre' => "MATHEX",
        'auteur' => 'mathex',
        'URL' => 'https://www.mathexien.com/index.php',
        'commentaire' => "Un ensemble de cours d'informatique et de mathématiques au lycée.",
        'icon' => 'fa fa-globe',
        'prerequis' => NULL
    )
);

// Ressources transversales 

register_activity(
    'unplugged',
    array(
        'category' => 'PROFS',
        'section' => 'PROFSTransversal',
        'type' => 'url',
        'titre' => "Activités débranchées",
        'auteur' => '',
        'URL' => 'https://classic.csunplugged.org/',
        'commentaire' => "Activités informatiques débranchées utiles au lycée (en anglais)",
        'image' => 'assets/img/cs-unplugged-classic-logo.png',
        'prerequis' => NULL
    )
);


register_activity(
    'Duflot',
    array(
        'category' => 'PROFS',
        'section' => 'PROFSTransversal',
        'type' => 'url',
        'titre' => "Les activités de médiation de M. DUFLOT-KREMER",
        'auteur' => 'Marie DUFLOT-KREMER',
        'URL' => 'https://members.loria.fr/MDuflot/files/med/index.html',
        'commentaire' => "Marie DUFLOT-KREMER propose tout un ensemble d'activité pour aborder en débranché la pensée informatique, comme le routage élastique, les marmottes au sommeil léger et le crépier psychorigide (pour n'en citer que quelques-une)",
        'icon' => 'fa fa-globe',
        'prerequis' => NULL
    )
);

