<?php

register_section(
    'NSI1histoire',
    array(
        'category' => 'NSI1',
        'titre' => "Histoire de l'informatique",
        'commentaire' => "Comme toute connaissance scientifique et technique, les concepts de l’informatique ont une histoire et ont été forgés par des personnes. Les algorithmes sont présents dès l’Antiquité..."
    )
);

register_section(
    'NSI1typebase',
    array(
        'category' => 'NSI1',
        'titre' => "Représentation des données: types et valeurs de base",
        'commentaire' => "Toute machine informatique manipule une représentation des données dont l’unité minimale est le bit, ce qui permet d’unifier logique et calcul. Les données  de  base sont représentées selon un codage dépendant de leur nature:  entiers,  flottants, caractères et chaînes de caractères."
    )
);

register_section(
    'NSI1typeconstruit',
    array(
        'category' => 'NSI1',
        'titre' => "Représentation des données: types construits",
        'commentaire' => "À partir des types de base se constituent des types construits (p-uplets, tableaux, dictionnaires,etc.)"
    )
);

register_section(
    'NSI1tables',
    array(
        'category' => 'NSI1',
        'titre' => "Traitement de données en tables",
        'commentaire' => "Les données organisées en table correspondent à une liste de p-uplets nommés qui partagent les mêmes descripteurs. La mobilisation de ce type de structure de données permet de préparer les élèves à aborder la notion de base de données qui ne sera présentée qu’en classe terminale."
    )
);

register_section(
    'NSI1ihm',
    array(
        'category' => 'NSI1',
        'titre' => "Interactions entre l’homme et la machine sur le Web",
        'commentaire' => "Lors de leur navigation sur le Web, les internautes interagissent avec leur machine par le biais des pages web. L’Interface Homme-Machine (IHM) repose sur la  gestion d’événements associés à des éléments graphiques munis de méthodes algorithmiques."
    )
);

register_section(
    'NSI1hardos',
    array(
        'category' => 'NSI1',
        'titre' => "Architectures matérielles et systèmes d'exploitation",
        'commentaire' => "Cette partie du programme étudie l'architecture des machines ainsi que les systèmes d'exploitations qui permettent leur fonctionnement."
    )
);

register_section(
    'NSI1langages',
    array(
        'category' => 'NSI1',
        'titre' => "Langages et programmation",
        'commentaire' => "On étudie en première les constructions élémentaires des langages, en python, ainsi que les bases du javascript, des langages HTML et CSS"
    )
);

register_section(
    'NSI1algo',
    array(
        'category' => 'NSI1',
        'titre' => "Algorithmique",
        'commentaire' => "Le concept de méthode algorithmique est introduit en classe de première et quelques algorithmes classiques sont étudiés."
    )
);



// Histoire
register_activity(
    'histoireCNRS',
    array(
        'category' => 'NSI1',
        'section' => 'NSI1histoire',
        'titre' => "Histoire de l'informatique, de 1945 à nos jours",
        'auteur' => 'Pierre Mounier-Kuhn (CNRS)',
        'youtubeId' => 'dcN9QXxmRqk',
        'commentaire' => "Un documentaire sur l'évolution de l'informatique de 1945 à nos jours.",
        'prerequis' => NULL
    )
);

register_activity(
    'histoireDelmas',
    array(
        'category' => 'NSI1',
        'section' => 'NSI1histoire',
        'type' => 'url',
        'titre' => "Histoire de l'informatique",
        'auteur' => 'Yannis DELMAS-RIGOUTSOS',
        'URL' => 'https://delmas-rigoutsos.nom.fr/documents/YDelmas-histoire_informatique/index.html',
        'commentaire' => "Un site d'un enseignant de l'université de Poitiers sur l'histoire de l'informatique, d'internet et du web.",
        'icon' => 'fa fa-globe',
        'prerequis' => NULL
    )
);

register_activity(
    'histoireTechno1',
    array(
        'category' => 'NSI1',
        'section' => 'NSI1histoire',
        'titre' => "Histoire de l'informatique, première partie",
        'auteur' => 'Technologie Collège Roujan',
        'youtubeId' => 'dJdiSN9q5QE',
        'commentaire' => "Un documentaire sur l'histoire de l'informatique, première partie.",
        'prerequis' => NULL
    )
);

register_activity(
    'histoireTechno2',
    array(
        'category' => 'NSI1',
        'section' => 'NSI1histoire',
        'titre' => "Histoire de l'informatique, seconde partie",
        'auteur' => 'Technologie Collège Roujan',
        'youtubeId' => 'NNxAKALRePo',
        'commentaire' => "Un documentaire sur l'histoire de l'informatique, seconde partie.",
        'prerequis' => NULL
    )
);

// hardos
register_activity(
    'IPetMask',
    array(
        'category' => 'NSI1',
        'section' => 'NSI1hardos',
        'titre' => "Réseaux : adresse IP et masques de sous-réseaux",
        'auteur' => 'Ludovic GUÉRIN',
        'youtubeId' => 'RnpSaDSSjR4',
        'commentaire' => "Cette vidéo traite du fonctionnement logique des réseaux. Elle traite notamment les notions d'adresse IP et de masques de sous-réseau ainsi que les calculs de plages adressables.",
        'prerequis' => NULL
    )
);

register_activity(
    'LogicGates',
    array(
        'category' => 'NSI1',
        'section' => 'NSI1hardos',
        'titre' => "Les portes logiques (Logic Gates)",
        'auteur' => ' PBElectronique',
        'youtubeId' => 'ExqPfs7Tu24',
        'commentaire' => "La base de l’électronique numérique est fondée sur l’algèbre de Boole. Les fonctions logiques directement issues de l’algèbre de Boole sont les outils de base de l'électronique numérique. Elles sont mises en oeuvre en électronique sous forme de portes logiques.",
        'prerequis' => NULL
    )
);
