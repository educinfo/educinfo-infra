<?php

require_once('lib/site/functions.php');

register_category(
	'SNT',
	array(
		'titre' => 'Sciences numériques et technologie',
		'commentaire' =>	"Voici un ensemble de liens utiles pour l'enseignement de seconde SNT"
	)
);

register_category(
	'NSI1',
	array(
		'titre' => 'Numérique et sciences informatique - 1<sup>ère</sup>',
		'commentaire' => "Voici un ensemble de lien en relation avec l'enseignement de la spécialité NSI en classe de première"
	)
);

register_category(
	'NSIT',
	array(
		'titre' => 'Numérique et sciences informatique - Terminale',
		'commentaire' => "Voici un ensemble de lien en relation avec l'enseignement de la spécialité NSI en classe de terminale"
	)
);

register_category(
	'GEN',
	array(
		'titre' => 'Enseignement général',
		'commentaire' => "Voici un ensemble de lien en relation avec l'informatique dans les différents enseignements généraux du lycée."
	)
);

register_category(
	'PROFS',
	array(
		'titre' => 'Ressources pour les enseignants',
		'commentaire' => "Voici un ensemble de lien destinés aux enseignants, en relation avec l'enseignement de l'informatique au lycée."
	)
);

$pages = array(
	'accueil' => array(
		"template" => 'welcome.twig.html',
		'titre' => 'Acceuil du site',
		'css' => 'style/site.min.css'
	),
	'SNT' => array(
		"template" => 'ressources.twig.html',
		'titre' => 'Sciences Numériques et Technologie',
		'category' => 'SNT'
	),
	'NSI1' => array(
		"template" => 'ressources.twig.html',
		'titre' => 'NSI 1ère',
		'category' => 'NSI1'
	),
	'NSIT' => array(
		"template" => 'ressources.twig.html',
		'titre' => 'NSI Terminale',
		'category' => 'NSIT'
	),
	'GEN' => array(
		"template" => 'ressources.twig.html',
		'titre' => 'Enseignement général',
		'category' => 'GEN'
	),
	'PROFS' => array(
		"template" => 'ressources.twig.html',
		'titre' => 'Ressources pour les enseignants',
		'category' => 'PROFS'
	),
	'progression1ere' => array(
		"template" => 'progression_premiere.twig.html',
		'titre' => 'Progression de Première',
		'category' => 'none'
	),
	'progressionTerminale' => array(
		"template" => 'progression_terminale.twig.html',
		'titre' => 'Progression de terminale',
		'category' => 'none'
	),
	'try' => array(
		"template" => 'try.twig.html',
		'titre' => 'Essai Python',
		'category' => 'none'
	),
	'empty' => array(
		"template" => 'empty.twig.html',
		'titre' => 'page vide',
		'category' => 'none'
	)
);
