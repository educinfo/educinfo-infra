# Changelog
educinfo-infrastructure

Package d'infrastructure pour le site educinfo.

Tous les changements notables seront documentés dans ce fichier

Le format s'appuie sur [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
et ce projet adhère au [versionnement sémantique](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Modification
 - passage à pyodide et typescript
 - nettoyage du javascript (encore)
 - **cours.css** : modification mineure de la mise en page en fonction de la taille des écrans.
 - **cours.css** : ajout d'une css pour les corrections 
 - **cours.css** : ajout d'une css pour les tables qui ne doivent pas faire 100% de large (.table-nonfluid)

## [0.1.0]
 - ajout de basthon pour les cours en python (mode basthon module-script)
 - ajout de blockly pour le module basthon
 - modification du fichier template de base
 - ajout de la police firacode pour l'éditeur
 - ajout du mode midnight tomorrow bright pour l'éditeur
 - modification de certains sources javascript pour la conformité au linter